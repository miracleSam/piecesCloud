package com.uestc.piecescloud;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.controller.OriginFileController;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.OBJECT_NOT_EXIST;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: 红雨澜山梦
 * @Date: 2019/3/30 0:18
 * @Version: 0.0.1
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SQL {
    @Autowired
    private OriginFileService originFileService;
    @Autowired
    private OriginFileController originFileController;
    @Test
    public void testOFS_queryFileParts() {
        Map<CLOUD, List<FilePart>> result = originFileService.queryFileParts(2309);
//        for (Map.Entry<CLOUD, List<FilePart>> entry : result.entrySet()) {
//            System.out.println(
//                    entry.getValue().get(0).getOriginFile().getOriginFileByteSize());
//        }
        System.out.println(JSON.toJSONString(result));
    }

    @Test
    public void testOFS_queryAll() {
//        List<Map<String, Object>> result = originFileService.queryAllOriginFile("all");
//        System.out.println(JSON.toJSONString(result));
    }

    @Test
    public void testOFC_download() {
//        String[] args = {};
//        ApplicationContext ctx = SpringApplication.run(Main.class, args);
//        Map<Integer, String> data = new HashMap<>();
//        data.put(2307,"1.pdf");
//        data.put(2308,"2.pdf");
//        data.put(2309,"3.pdf");
//        originFileController.download(JSON.toJSONString(data));
    }
}
