package com.uestc.piecescloud;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.bos.BosClient;
import com.baidubce.services.bos.BosClientConfiguration;
import com.uestc.piecescloud.service.cloud.CloudUserConfiguration;
import com.uestc.piecescloud.service.cloud.baidu.BaiduUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

/**
 * @Description: 云平台提供商的接口单元测试$
 * @Author: 红雨澜山梦
 * @create: 2019-04-01 16:15
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Cloud {
    /*
     * 对象属性
     */
    private String ACCESS_KEY_ID = CloudUserConfiguration.getBaiduAKI();
    private String SECRET_ACCESS_KEY = CloudUserConfiguration.getBaiduSAK();
    private String ENDPOINT = CloudUserConfiguration.getBaiduEP();
    private String bucketName;
    private BaiduUtil baiduUtil;

    @Before
    public void init() {

        this.bucketName = CloudUserConfiguration.getBaiduBucketName();
        baiduUtil = new BaiduUtil(ACCESS_KEY_ID, SECRET_ACCESS_KEY, ENDPOINT);
    }

    /**
     * BosClient初始化
     *
     * @return bosclient
     */
    private BosClient bosClientInit(String ACCESS_KEY_ID, String SECRET_ACCESS_KEY, String ENDPOINT) {
        // 初始化一个BosClient
        BosClientConfiguration config = new BosClientConfiguration();
        config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
        config.setEndpoint(ENDPOINT);
        // 设置HTTP最大连接数为10
        config.setMaxConnections(10);
        // 设置TCP连接超时为5000毫秒
        config.setConnectionTimeoutInMillis(5000);
        // 设置Socket传输数据超时的时间为20000毫秒
        config.setSocketTimeoutInMillis(20000);
        BosClient client = new BosClient(config);
        return client;
    }

    @Test
    public void resumeUpload() {
        File f = new File("/home/baotao/Downloads/angular-1.5.8.zip");
        System.out.println("asdasd");
        baiduUtil.resumeBrokenUpload_r(f, f.getName(), this.bucketName);
        System.out.println("tetst");
    }

}
