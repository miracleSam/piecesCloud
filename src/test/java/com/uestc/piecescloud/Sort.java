package com.uestc.piecescloud;

import com.uestc.piecescloud.bean.FilePart;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @Description: 排序的单元测试$
 * @Author: 红雨澜山梦
 * @create: 2019-04-01 22:16
 */
public class Sort {

    @Test
    public void FilePartSequenceSort() {

        List<FilePart> fileParts = new ArrayList<>();

        FilePart filePart1 = new FilePart();
        filePart1.setSequence(8);

        FilePart filePart2 = new FilePart();
        filePart2.setSequence(1);

        FilePart filePart3 = new FilePart();
        filePart3.setSequence(10);

        fileParts.add(filePart1);
        fileParts.add(filePart3);
        fileParts.add(filePart2);

        for (FilePart fp : fileParts) {
            System.out.println(fp.getSequence());
        }
        // 正序
        fileParts.sort(Comparator.naturalOrder());
        for (FilePart fp : fileParts) {
            System.out.println(fp.getSequence());
        }
    }
}
