package com.uestc.piecescloud.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * 对文件，String进行MD5的操作
 *
 * @author 红雨澜山梦
 * @date 2018-04-10 上午10:10:39
 */
public class MD5Utils {

    private static Logger logger = LogManager.getLogger(MD5Utils.class);

    /**
     * 计算文件的hash
     *
     * @param filePath 文件绝对路径
     * @return 文件的hash值
     */
    public static String md5HashCode32(String filePath) {
        FileInputStream fileis = null;
        try {
            fileis = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            logger.error("File:" + filePath + "不存在，可能已经被删除，或者转移到其他文件夹下。");
            e.printStackTrace();
        }
        return md5HashCode32(fileis);
    }

    /**
     * 计算文件的hash
     *
     * @param file 待hash的文件
     * @return 文件的hash值
     */
    public static String md5HashCode32(File file) {
        FileInputStream fileis = null;
        try {
            fileis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            logger.error("File:" + file.getAbsolutePath() + "不存在，可能已经被删除，或者转移到其他文件夹下。");
            e.printStackTrace();
        }
        return md5HashCode32(fileis);
    }


    /**
     * 计算文件流的hash
     *
     * @param fileis 输入流
     * @return 该输入流的hash值
     */
    public static String md5HashCode32(InputStream fileis) {
        try {
            //拿到一个MD5转换器,如果想使用SHA-1或SHA-256，则传入SHA-1,SHA-256    
            MessageDigest md = MessageDigest.getInstance("MD5");

            //分多次将一个文件读入，对于大型文件而言，这种方式占用内存比较少。  
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fileis.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            fileis.close();

            //转换并返回包含16个元素字节数组,返回数值范围为-128到127  
            byte[] md5Bytes = md.digest();
            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;//解释参见最下方  
                if (val < 16) {
                    /**
                     * 如果小于16，那么val值的16进制形式必然为一位， 
                     * 因为十进制0,1...9,10,11,12,13,14,15 对应的 16进制为 0,1...9,a,b,c,d,e,f; 
                     * 此处高位补0。 
                     */
                    hexValue.append("0");
                }
                //这里借助了Integer类的方法实现16进制的转换   
                hexValue.append(Integer.toHexString(val));
            }
            return hexValue.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 获取String的MD5值
     * (此方法用来处理获得的MAC地址)
     *
     * @param info 字符串
     * @return 该字符串的MD5值
     * @author wmxl
     * @date 2018-04-20 下午17:35:00
     */
    public static String MD5StringHashCode32(String info) {
        try {
            //获取 MessageDigest 对象，参数为 MD5 字符串，表示这是一个 MD5 算法（其他还有 SHA1 算法等）：
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //update(byte[])方法，输入原数据
            //类似StringBuilder对象的append()方法，追加模式，属于一个累计更改的过程
            md5.update(info.getBytes("UTF-8"));
            //digest()被调用后,MessageDigest对象就被重置，即不能连续再次调用该方法计算原数据的MD5值。可以手动调用reset()方法重置输入源。
            //digest()返回值16位长度的哈希值，由byte[]承接
            byte[] md5Array = md5.digest();
            //byte[]通常我们会转化为十六进制的32位长度的字符串来使用,本文会介绍三种常用的转换方法
            return bytesToHex1(md5Array);
        } catch (NoSuchAlgorithmException e) {
            return "";
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /*
     * 此方法服务上面的方法
     */
    private static String bytesToHex1(byte[] md5Array) {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < md5Array.length; i++) {
            int temp = 0xff & md5Array[i];//TODO:此处为什么添加 0xff & ？
            String hexString = Integer.toHexString(temp);
            if (hexString.length() == 1) {//如果是十六进制的0f，默认只显示f，此时要补上0
                strBuilder.append("0").append(hexString);
            } else {
                strBuilder.append(hexString);
            }
        }
        return strBuilder.toString();
    }


    /*
     * 方法md5HashCode32 中     ((int) md5Bytes[i]) & 0xff   操作的解释：
     * 在Java语言中涉及到字节byte数组数据的一些操作时，经常看到 byte[i] & 0xff这样的操作，这里就记录总结一下这里包含的意义：
     * 1、0xff是16进制（十进制是255），它默认为整形，二进制位为32位，最低八位是“1111 1111”，其余24位都是0。
     * 2、&运算: 如果2个bit都是1，则得1，否则得0；
     * 3、byte[i] & 0xff：首先，这个操作一般都是在将byte数据转成int或者其他整形数据的过程中；使用了这个操作，最终的整形数据只有低8位有数据，其他位数都为0。
     * 4、这个操作得出的整形数据都是大于等于0并且小于等于255的
     */

}
