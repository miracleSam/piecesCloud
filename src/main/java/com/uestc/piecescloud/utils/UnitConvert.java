package com.uestc.piecescloud.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * @Description: 用于单位之间的转换$
 * @Author: 红雨澜山梦
 * @create: 2019-03-13 14:22
 */
public class UnitConvert {

    public static double getSpeed(long start, long end, long byteSize, String speedUnit) {
        long time = end - start;
        double DbyteSize = (double) byteSize;
//		if(time < 1)
//			time = 1;
        // 默认为B/s
        switch (speedUnit.toLowerCase()) {
            case "b/s":
//			speed = speed;
                break;
            case "kb/s":
                DbyteSize = DbyteSize / 1024;
                break;
            case "mb/s":
                DbyteSize = DbyteSize / (1024 * 1024);
                break;
            default:
                DbyteSize = DbyteSize / 1024;
                break;
        }
        double speed = new BigDecimal(DbyteSize / ((double) time / (double) 1000)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println("speed=" + speed);
        return speed;
    }

    /**
     * 返回给前端的源文件的字节大小
     *
     * @param byteSize
     * @return
     */
    public static String convertByteSize(long byteSize) {
        String str = "";
        double DbyteSize = (double) byteSize;
        // B:0 KB:1 MB:2 TB:3
        int level = 0;
        // 取1000为节点，从客户的角度上来看1014KB不如0.99M好
        while (DbyteSize > 1000) {
            DbyteSize = DbyteSize / 1024.0;
            level++;
        }
        // 取2位小数，并且进行四舍五入
        DbyteSize = new BigDecimal(DbyteSize).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        str = "" + DbyteSize;
        switch (level) {
            case 0:
                str += "B";
                break;
            case 1:
                str += "KB";
                break;
            case 2:
                str += "MB";
                break;
            case 3:
                str += "GB";
                break;
            default:
                str = "---";
        }
        return str;
    }

    /**
     * 小数点转百分比字符串
     *
     * @param f
     * @return
     */
    public static String floatToPercent(double f) {
        NumberFormat numberFormat = NumberFormat.getPercentInstance();
        numberFormat.setMaximumFractionDigits(4); //精确到4位
        return numberFormat.format(f);
    }

}