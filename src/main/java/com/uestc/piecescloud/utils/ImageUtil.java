package com.uestc.piecescloud.utils;

import com.uestc.piecescloud.constant.Constant;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 图片压缩等，目前代码上只支持单个处理
 */
public class ImageUtil {

    private static String inputFilepath = Constant.HSB_BUFFER_PATH;
    private static String outFilepath = Constant.HSB_TMP_VIEW_PATH;

    /**
     * 指定大小进行缩放（此方法不改变图片比例）
     *
     * @throws IOException
     */
    public static void handleBySize(int width, int height, File inputFile, File outputFile) throws IOException {
        /*
         * size(200,300) 若图片横比200小，高比300小，不变
         * 若图片横比200小，高比300大，高缩小到300，图片比例不变 若图片横比200大，高比300小，横缩小到200，图片比例不变
         * 若图片横比200大，高比300大，图片按比例缩小，横为200或高为300
         */
        Thumbnails.of(inputFile).size(200, 300).toFile(outputFile);
    }

    /**
     * 按照比例进行缩放
     *
     * @throws IOException
     */
    public static void handleByScale(String fileName, float scale) throws IOException {
        /**
         * scale(比例)
         */
        Thumbnails.of(inputFilepath + File.separator + fileName).scale(scale).toFile(outFilepath + File.separator + fileName);
    }

    /**
     * 不按照比例，指定大小进行缩放（此方法会改变比例）
     *
     * @throws IOException
     */
    public static void handleBySizeWithoutScale(String fileName, int width, int height) throws IOException {
        /**
         * keepAspectRatio(false) 默认是按照比例缩放的
         */
        Thumbnails.of(inputFilepath + File.separator + fileName).size(120, 120).keepAspectRatio(false).toFile(outFilepath + File.separator + fileName);
    }

//    /**
//     * 旋转
//     *
//     * @throws IOException
//     */
//    private void rotate(String path, int angle, String savedPath) throws IOException {
//        /**
//         * rotate(角度),正数：顺时针 负数：逆时针
//         */
//        Thumbnails.of("images/test.jpg").size(1280, 1024).rotate(90).toFile("C:/image+90.jpg");
//        Thumbnails.of("images/test.jpg").size(1280, 1024).rotate(-90).toFile("C:/iamge-90.jpg");
//    }
//
//    /**
//     * 水印
//     *
//     * @throws IOException
//     */
//    private void watermark(String path, int angle, String savedPath) throws IOException {
//        /**
//         * watermark(位置，水印图，透明度)
//         */
//        Thumbnails.of("images/test.jpg").size(1280, 1024).watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File("images/watermark.png")), 0.5f)
//                .outputQuality(0.8f).toFile("C:/image_watermark_bottom_right.jpg");
//        Thumbnails.of("images/test.jpg").size(1280, 1024).watermark(Positions.CENTER, ImageIO.read(new File("images/watermark.png")), 0.5f)
//                .outputQuality(0.8f).toFile("C:/image_watermark_center.jpg");
//    }
//
//    /**
//     * 裁剪
//     *
//     * @throws IOException
//     */
//    private void cut() throws IOException {
//        /**
//         * 图片中心400*400的区域
//         */
//        Thumbnails.of("images/test.jpg").sourceRegion(Positions.CENTER, 400, 400).size(200, 200).keepAspectRatio(false)
//                .toFile("C:/image_region_center.jpg");
//        /**
//         * 图片右下400*400的区域
//         */
//        Thumbnails.of("images/test.jpg").sourceRegion(Positions.BOTTOM_RIGHT, 400, 400).size(200, 200).keepAspectRatio(false)
//                .toFile("C:/image_region_bootom_right.jpg");
//        /**
//         * 指定坐标
//         */
//        Thumbnails.of("images/test.jpg").sourceRegion(600, 500, 400, 400).size(200, 200).keepAspectRatio(false).toFile("C:/image_region_coord.jpg");
//    }
//
//    /**
//     * 转化图像格式
//     *
//     * @throws IOException
//     */
//    private void changeType() throws IOException {
//        /**
//         * outputFormat(图像格式)
//         */
//        Thumbnails.of("images/test.jpg").size(1280, 1024).outputFormat("png").toFile("C:/image_1280x1024.png");
//        Thumbnails.of("images/test.jpg").size(1280, 1024).outputFormat("gif").toFile("C:/image_1280x1024.gif");
//    }
//
//    /**
//     * 输出到OutputStream
//     *
//     * @throws IOException
//     */
//    private void toOutputStream() throws IOException {
//        /**
//         * toOutputStream(流对象)
//         */
//        OutputStream os = new FileOutputStream("C:/image_1280x1024_OutputStream.png");
//        Thumbnails.of("images/test.jpg").size(1280, 1024).toOutputStream(os);
//    }
//
//    /**
//     * 输出到BufferedImage
//     *
//     * @throws IOException
//     */
//    private void toBUfferedImage() throws IOException {
//        /**
//         * asBufferedImage() 返回BufferedImage
//         */
//        BufferedImage thumbnail = Thumbnails.of("images/test.jpg").size(1280, 1024).asBufferedImage();
//        ImageIO.write(thumbnail, "jpg", new File("C:/image_1280x1024_BufferedImage.jpg"));
//    }


//    public static void main(String[] args) throws IOException {
//        String shutu = Constant.HSB_BUFFER_PATH + File.separator + "1.jpg";
//        String hengtu = Constant.HSB_BUFFER_PATH + File.separator + "2.jpg";
//        String savedPath = Constant.HSB_TMP_VIEW_PATH;
//
//        ImageUtil imageUtil = new ImageUtil();
//
//        imageUtil.handleBySize("2.jpg",100,200);
//
//    }


}
