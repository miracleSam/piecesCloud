package com.uestc.piecescloud.utils;

/**
 * @Description: 涉及时间的一些共用操作方法$
 * @Author: 红雨澜山梦
 * @create: 2019-02-28 17:40
 */
public class Time {

    /**
     * 线程休眠time毫秒
     *
     * @param time 休眠的毫秒数
     */
    public static void threadSleepInTimeMillis(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
