package com.uestc.piecescloud.controller;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.constant.Constant;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * @Description: 临时创建，用于开发者通过HTTP管理HSB$
 * @Author: 红雨澜山梦
 * @create: 2019-05-03 20:16
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @RequestMapping(value = "/test")
    public String test(){
        return "dashboard";
    }
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public void getFileToDesktop(HttpServletRequest request, HttpServletResponse response) {
//        String uploadPath = "/home/pi/Desktop/";
        String uploadPath = "D:\\temp";

        PrintWriter out = null;
        response.setContentType("text/html;charset=UTF-8");

        Map map = new HashMap();
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        File directory = null;
        List<FileItem> items = new ArrayList<>();
        try {
            items = upload.parseRequest(request);
            // 得到所有的文件
            Iterator<FileItem> it = items.iterator();
            while (it.hasNext()) {
                FileItem fItem = (FileItem) it.next();
                String fName = "";
                Object fValue = null;
                if (fItem.isFormField()) { // 普通文本框的值
                    fName = fItem.getFieldName();
                    //                  fValue = fItem.getString();
                    fValue = fItem.getString("UTF-8");
                    map.put(fName, fValue);
                } else { // 获取上传文件的值
                    fName = fItem.getFieldName();
                    fValue = fItem.getInputStream();
                    map.put(fName, fValue);
                    String name = fItem.getName();
                    if (name != null && !("".equals(name))) {
                        name = name.substring(name.lastIndexOf(File.separator) + 1);

                        //String stamp = StringUtils.getFormattedCurrDateNumberString();

                        directory = new File(uploadPath);
                        directory.mkdirs();

                        String filePath = uploadPath + File.separator + name;
                        map.put(fName + "FilePath", filePath);

                        InputStream is = fItem.getInputStream();
                        FileOutputStream fos = new FileOutputStream(filePath);
                        byte[] buffer = new byte[1024];
                        while (is.read(buffer) > 0) {
                            fos.write(buffer, 0, buffer.length);
                        }
                        fos.flush();
                        fos.close();
                        map.put(fName + "FileName", name);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("读取http请求属性值出错!");
            //          e.printStackTrace();
        }

        // 数据处理
        try {
            out = response.getWriter();
            out.print("{success:true, msg:'接收成功'}");
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //TODO 下载多个文件的时候，如何从服务端发送到移动端
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public void Download(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) {

        String filename = String.valueOf(((Map<String, Object>) JSON.parseObject(data)).get("filename"));
        //取body中数据的操作
        System.out.println("begin ====  download");
        if (data == null) {
            filename = "downloadTEST.txt";
        }
        returnToWechat(response, filename);

        System.out.println("end ====  download");

    }

    private void returnToWechat(HttpServletResponse response, String fileName) {

        response.setContentType("application/x-download");
        String downloadPath = Constant.WECHAT_DOWNLOAD_PATH;
//        fileName = URLEncoder.encode(fileName,"UTF-8");//（如果文件名称加密，使用此代码具有解密功能）
        response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
        OutputStream outp = null;
        FileInputStream in = null;
        try {
            outp = response.getOutputStream();
            in = new FileInputStream(downloadPath + File.separator + fileName);
            byte[] b = new byte[1024];
            int i = 0;
            while ((i = in.read(b)) > 0) {
                outp.write(b, 0, i);
            }
            System.out.println(downloadPath + File.separator + fileName);
            outp.flush();
        } catch (Exception e) {
            System.out.println("Error!");
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                in = null;
            }
            if (outp != null) {
                try {
                    outp.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                outp = null;
            }
        }
    }

}
