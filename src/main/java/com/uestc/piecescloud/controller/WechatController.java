package com.uestc.piecescloud.controller;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.ContentType;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.fileoperation.FileDestroy;
import com.uestc.piecescloud.service.fileoperation.FileDownload;
import com.uestc.piecescloud.service.fileoperationbyuser.FileService;
import com.uestc.piecescloud.utils.ImageUtil;
import com.uestc.piecescloud.utils.Time;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.eclipse.jetty.client.Origin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.uestc.piecescloud.utils.AESpkcs7paddingUtil;

/**
 * @Description: 用于对微信小程序的上传和下载文件的功能$
 * @Author: 红雨澜山梦
 * @create: 2019-01-30 16:16
 */
@RestController
//@RequestMapping("/wechat")
public class WechatController {

    @Autowired
    private FileService fileService;

    /**
     * 实现文件上传
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@RequestParam("fileName") MultipartFile file, HttpSession session) {
        // 获取当前用户
        User user = getUserFromSession(session);
        if (user == null){return "";}

        System.out.println("user:"+user.getOpenid());
        if (file.isEmpty()) {
            return "false";
        }
        int size = (int) file.getSize();
        System.out.println(file.getOriginalFilename() + "-->" + size);

//        String path = Constant.WECHAT_UPLOAD_PATH;
        String path = Constant.HSB_USER + File.separator + user.getOpenid() + Constant.HSB_USER_UPLOAD;
        System.out.println(path);
        File dest = new File(path + File.separator + file.getOriginalFilename());
        if (!dest.getParentFile().exists()) { //判断文件父目录是否存在
            dest.getParentFile().mkdir();
        }

        try {
//            file.transferTo(dest); //保存文件 该方法return后 会删除dest所指向的文件
            // 保存文件
            dest.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(dest);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
            // 启动上传服务
            fileService.upload(user, dest);

            System.out.println(dest.exists());
            System.out.println(dest.isFile());

            System.out.println(dest.getAbsolutePath());

//            Time.threadSleepInTimeMillis(10000);

            return "true";
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "false";
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "false";
    }

//    /**
//     * 实现多文件上传
//     */
//    @RequestMapping(value = "multifileUpload", method = RequestMethod.POST)
//    public String multifileUpload(HttpServletRequest request) {
//
//        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("fileName");
//
//        if (files.isEmpty()) {
//            return "false";
//        }
//
//        String path = Constant.WECHAT_UPLOAD_PATH;
//
//        for (MultipartFile file : files) {
//            String fileName = file.getOriginalFilename();
//            int size = (int) file.getSize();
//            System.out.println(fileName + "-->" + size);
//
//            if (file.isEmpty()) {
//                return "false";
//            } else {
//                File dest = new File(path + File.separator + fileName);
//                if (!dest.getParentFile().exists()) { //判断文件父目录是否存在
//                    dest.getParentFile().mkdir();
//                }
//                try {
////                    file.transferTo(dest);
//                    // 保存文件
//                    dest.createNewFile();
//                    FileOutputStream fileOutputStream = new FileOutputStream(dest);
//                    fileOutputStream.write(file.getBytes());
//                    fileOutputStream.close();
//
//                } catch (Exception e) {
//
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    return "false";
//                }
//            }
//        }
//        return "true";
//    }

    /**
     *
     * @param transedSuccessFMIDs
     * @return
     */
    @RequestMapping(value = "clearTransedInfo", method = RequestMethod.POST)
    public String clearTransedInfo(@RequestBody String transedSuccessFMIDs) {
//        System.out.println(transedSuccessFMIDs);
        List<String> FMIDs = ((Map<String, List<String>>) JSON.parse(transedSuccessFMIDs)).get("transedSuccessFMIDs");
        for (String FMID : FMIDs) {
            // 目前只清除上传完成的文件。下载文件 为手动清除
            if (FILE_STATUS.isUP(
                    FileManager.getUploadMap().get(FMID).get(Constant.FILE_MANAGER_CODE).toString())) {
                File file = new File(Constant.HSB_FILE_OPERATRION_PATH + File.separator + String.valueOf(
                        FileManager.getUploadMap().get(FMID).get(Constant.FILE_MANAGER_ORIGINFILENAME)));
                System.out.println("删除已经上传的文件：" + file.getAbsolutePath());
                FileDestroy.destroy(file);
            }

//            System.out.println("FMID = " + FMID);
            FileManager.remove(FMID);
        }
        return "success";
    }

    /**
     * 下载单个文件
     *
     * @param files
     * @param response
     */
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void download(@RequestParam(name = "file") String files, HttpServletResponse response, HttpSession session) {
        User user = getUserFromSession(session);
        System.out.println("user:"+JSON.toJSONString(user)+"请求下载"+files);
        Map<String, Object> fileIDNames = (Map<String, Object>) JSON.parse(files);
        String filePath = Constant.HSB_USER + File.separator + user.getOpenid() + Constant.HSB_USER_DOWNLOAD;
        // step 1 : 取body中的id和name
        // 此处只有一个键值对
        String filename = null;
        int id = 0;
        for (String IDs : fileIDNames.keySet()) {
            id = Integer.valueOf(IDs);
            filename = String.valueOf(fileIDNames.get(IDs));
            System.out.println(IDs + "" + filename);
            break; // 为了防止下载出错，此处break保证只有一个ID:filename
        }
        // step 2 : 根据取出的id，下载对应的文件
        File file = new File(filePath + File.separator + filename);
//        System.out.println("file:"+file.length());
        OriginFile originFile = new OriginFile(file);
        originFile.setOriginFileId(id);
        System.out.println("downloading *****************"+JSON.toJSONString(originFile));
        fileService.download(user, originFile);

//        System.out.println("file.exists()");
        if (file.exists()) {

            System.out.println("file:"+file.length());
            // step 3 : 根据文件名，获取后缀，设置contentType
            String suffix = filename.substring(filename.lastIndexOf("."));
            response.setContentType(ContentType.getContentType(suffix));
            response.setHeader("Content-Disposition", "attachment;fileName=" + filename);

            byte[] buffer = new byte[1024];
            FileInputStream fis = null; //文件输入流
            BufferedInputStream bis = null;

            OutputStream os = null; //输出流
            // step 4 : 返回文件数据
            try {
                os = response.getOutputStream();
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer);
                    i = bis.read(buffer);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("----------file download" + filename);
            try {
                bis.close();
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * 主界面的缩略图
     * @param id
     * @param fileName
     * @param response
     */
    @RequestMapping(value = "/tmp/{id}/{fileName}", method = RequestMethod.GET)
    public void thumbnail(@PathVariable("id") Integer id, @PathVariable("fileName") String fileName,
                          @RequestParam("cipherOpenid") String cipherOpenid, HttpServletResponse response) {
        System.out.println("主界面缩略图");
        User user = new User();
        // TODO 目前cipherOpenid就是 明文的openid
        String openid = getOpenidFromCipher(cipherOpenid);
        user.setOpenid(openid);

        System.out.println("openid = " + openid);

        File outputFile = new File(Constant.HSB_USER + File.separator + openid + Constant.HSB_USER_TMP_VIEW
                + Constant.HSB_USER_TMP_VIEW_ALL + File.separator + id + "_" + fileName);
        if (!outputFile.exists()) {
            // 下载文件的地址
            File file = new File( Constant.HSB_USER + File.separator + openid + Constant.HSB_USER_DOWNLOAD
                    + File.separator + id + "_" + fileName);
            if (!file.exists()) {
                OriginFile originFile = new OriginFile(file);
                originFile.setOriginFileId(id);
                originFile.setThumbnail(true); // 表示此源文件为缩略图
                fileService.download(user, originFile);
            }
            try {
                // 下载后，直接压缩
                ImageUtil.handleBySize(200, 100, file, outputFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String suffix = fileName.substring(fileName.lastIndexOf("."));
        response.setContentType(ContentType.getContentType(suffix));
        response.setHeader("Content-Disposition", "attachment;fileName=" + outputFile.getName());

        byte[] buffer = new byte[1024];
        FileInputStream fis = null; //文件输入流
        BufferedInputStream bis = null;

        OutputStream os = null; //输出流
        try {
            os = response.getOutputStream();
//            System.out.println("fis = new FileInputStream(file);" + file.getAbsolutePath());
            fis = new FileInputStream(outputFile);
            bis = new BufferedInputStream(fis);
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer);
                i = bis.read(buffer);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        System.out.println("----------图片返回" + file.getName());
        try {
            bis.close();
            fis.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 上传的时候，传输列表的缩略图
     * @param fileName
     * @param response
     */
    @RequestMapping(value = "/tmp/up/{fileName}")
    public void thumbnail(@PathVariable("fileName") String fileName, HttpServletResponse response,
                          @RequestParam("cipherOpenid") String cipherOpenid) {
        String openid = getOpenidFromCipher(cipherOpenid);

        File outputFile = new File(Constant.HSB_USER + File.separator + openid + Constant.HSB_USER_TMP_VIEW
                + Constant.HSB_USER_TMP_VIEW_UP + File.separator + fileName);
        File file = new File(Constant.HSB_USER + File.separator + openid + Constant.HSB_USER_UPLOAD +
                File.separator + fileName);
        // 输出文件不存在，则压缩
        if(!outputFile.exists()) {
            try {
                ImageUtil.handleBySize(200, 100, file, outputFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String suffix = fileName.substring(fileName.lastIndexOf("."));

        // 这里暂时不压缩进入/HSBbuffer/tmp_view下
        response.setContentType(ContentType.getContentType(suffix));
        response.setHeader("Content-Disposition", "attachment;fileName=" + file.getName());

        byte[] buffer = new byte[1024];
        FileInputStream fis = null; //文件输入流
        BufferedInputStream bis = null;

        OutputStream os = null; //输出流
        try {
            os = response.getOutputStream();
//            System.out.println("fis = new FileInputStream(file);" + file.getAbsolutePath());
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            int i = bis.read(buffer);
            while (i != -1) {
                os.write(buffer);
                i = bis.read(buffer);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        System.out.println("----------图片返回" + file.getName());
        try {
            bis.close();
            fis.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private User getUserFromSession(HttpSession session) {
        if (session.getAttribute("openid") == null) {
            return new User();
        }
        return new User(session.getAttribute("openid").toString());
    }

    private String getOpenidFromCipher(String cipherOpenid) {
        return cipherOpenid;
    }

}
