package com.uestc.piecescloud.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.service.database.storage.UserService;
import com.uestc.piecescloud.service.fileoperationbyuser.FileService;
import org.eclipse.jetty.http.MetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import com.uestc.piecescloud.utils.AESpkcs7paddingUtil;

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RestTemplate restTemplate;

    private int i = 0;

    @Value("${wechat.app.appid}")
    private String appid;
    @Value("${wechat.app.secret}")
    private String secret;

    @RequestMapping("/login")
    public String login(@RequestParam String code, HttpSession session) {
        System.out.println(code);

        String url = "https://api.weixin.qq.com/sns/jscode2session?appid="+appid+"&secret="+secret+"&js_code="+code+"&grant_type=authorization_code";

        JSONObject info = JSON.parseObject(
                restTemplate.getForEntity(url, String.class).getBody());

        String openid = info.get("openid").toString();
        String session_key = info.get("session_key").toString();

//        String openid = "testopenid";
        User user = new User();
        user.setOpenid(openid);
        userService.login(user);

        session.setAttribute("openid", openid);
        session.setAttribute("session_key", session_key);
        String sessionId = session.getId();

        System.out.println("info"+JSON.toJSONString(info));
        System.out.println("sessionId:"+sessionId);

        Map<String, String> map = new HashMap<>();
        map.put("session_key", session_key);
        // TODO 目前openid以明文形式传输
        map.put("cipherOpenid", openid);
//        try {
//            String cipherOpenid = AESpkcs7paddingUtil.encrypt(openid, session_key);
//            map.put("cipherOpenid", cipherOpenid);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return JSON.toJSONString(map);

    }

//    public void register() {}

    public void modify() {}

    @RequestMapping(value = "/addFriend", method = RequestMethod.POST)
    public void addFriend(@RequestParam int userId1, @RequestParam int userId2) {
        userService.addFriend(userId1, userId2);
    }

    @RequestMapping("/show/getput")
    public String showGetPut(@RequestParam String wxid) {
        User user = new User();
        user.setWxid(wxid);
        return JSON.toJSONString(userService.showGetPut(user));
    }

    @RequestMapping("/show/space")
    public String showSpace(@RequestParam String wxid) {
        User user = new User();
        user.setWxid(wxid);
        return JSON.toJSONString(userService.showSpace(user));
    }

    @RequestMapping("/show/friends")
    public String showFriends(@RequestParam String wxid) {
        User user = new User();
        user.setWxid(wxid);
        return JSON.toJSONString(userService.showFriends(user));
    }

    @RequestMapping("/session")
    public void testForSession(@RequestParam("cipherOpenid")String cipherOpenid, HttpSession session) throws Exception {

//        String sessionId = session.getId();

//        String openid = String.valueOf(session.getAttribute("openid"));

//        System.out.println("sessionId:"+sessionId);
//        System.out.println("cipherOpenid:"+cipherOpenid);
//        try {
//            System.out.println("openid:"+ AESpkcs7paddingUtil.decrypt(cipherOpenid, session.getAttribute("session_key").toString()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "openid:" + AESpkcs7paddingUtil.decrypt(cipherOpenid, session.getAttribute("session_key").toString());
    }

}
