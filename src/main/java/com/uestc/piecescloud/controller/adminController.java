package com.uestc.piecescloud.controller;/*
 *@Description
 *@Author:Sam
 *@creat-time:2020/7/7 14:17
 */

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import com.uestc.piecescloud.service.fileoperationbyuser.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class adminController {
    @Autowired
    private OriginFileService originFileService;

    @Autowired
    private FileService fileService;

    @RequestMapping("")
    public String admin(HttpServletRequest httpRequest){
        HttpSession httpSession=httpRequest.getSession();
        httpSession.setAttribute("error",null);
        return "login";
    }
    @RequestMapping("/login")
    public String login(@RequestParam("userName") String userName, @RequestParam("password") String password, HttpServletRequest httpRequest){
        HttpSession httpSession=httpRequest.getSession();
        if(userName.equals("root405")&&password.equals("root405")){
            httpSession.setAttribute("userName",userName);
            httpSession.setAttribute("openid","admin");
            return "dashboard";
        }
        httpSession.setAttribute("error","账号或密码错误");
        return "login";
    }
    @RequestMapping("/upload")
    public String upload(HttpSession httpSession){
        httpSession.setAttribute("msg",null);
        return "upload";
    }
    @RequestMapping("/dashboard")
    public String dashboard(){
        return "dashboard";
    }
    @RequestMapping("/download")
    public String download(HttpSession session, ModelMap modelMap,HttpServletRequest httpServletRequest){
        User user = getUserFromSession(session);
        session.setAttribute("pageMax",false);
        System.out.println(httpServletRequest.getParameter("pageNum"));
        if(httpServletRequest.getParameter("pageNum")!=null){
            int pageNum=Integer.parseInt(httpServletRequest.getParameter("pageNum"));
            List<Map<String, Object>> result=originFileService.queryAllOriginFile("all" ,user,pageNum);
            modelMap.addAttribute("files",result);
            session.setAttribute("pageNum",pageNum);
            if(result.size()==0){
                session.setAttribute("pageMax",true);
            }
            return "download";
        }else {
            modelMap.addAttribute("files",originFileService.queryAllOriginFile("all" ,user,null));
            session.setAttribute("pageNum",1);
            return "download";
        }
    }
    //    /**
//     * 实现多文件上传
//     */
    @RequestMapping(value = "/multifileUpload", method = RequestMethod.POST)
    public String multifileUpload(HttpServletRequest request,HttpSession httpSession,ModelMap modelMap) {

        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("fileName");

        List results=new ArrayList();
        User user=getUserFromSession(httpSession);
        String path = Constant.HSB_USER + File.separator + user.getOpenid() + Constant.HSB_USER_UPLOAD;
        for (MultipartFile file : files) {
            String fileName = file.getOriginalFilename();
            int size = (int) file.getSize();
            System.out.println(fileName + "-->" + size);
            if (file.isEmpty()) {
                return "false";
            } else {
                File dest = new File(path + File.separator + file.getOriginalFilename());
                if (!dest.getParentFile().exists()) { //判断文件父目录是否存在
                    dest.getParentFile().mkdir();
                }
                try {
//                    file.transferTo(dest);
                    // 保存文件
                    dest.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(dest);
                    fileOutputStream.write(file.getBytes());
                    fileOutputStream.close();

                    // 启动上传服务
                    fileService.upload(user, dest);
                    System.out.println(dest.exists());
                    System.out.println(dest.isFile());
                    System.out.println(dest.getAbsolutePath());
                    OriginFile originFile=new OriginFile();
                    originFile.setAbsolutePath(dest.getAbsolutePath());
                    originFile.setOriginFileName(dest.getName());
                    results.add(originFile);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return "false";
                }
            }
        }
        modelMap.addAttribute("files",results);
        httpSession.setAttribute("msg","上传成功");
        return "upload";
    }
    private User getUserFromSession(HttpSession session) {
        if (session.getAttribute("openid") == null) {
            return null;
        }
        return new User(session.getAttribute("openid").toString());
    }
}
