package com.uestc.piecescloud.controller;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.Log;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.database.storage.CloudService;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import com.uestc.piecescloud.service.fileoperation.FileDownload;
import com.uestc.piecescloud.service.user.HistoryLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description: Controller层中，对应origin的路径映射$
 * @Author: 红雨澜山梦
 * @create: 2019-02-02 01:10
 */
@RestController
@RequestMapping("/origin")
public class OriginFileController {

    @Autowired
    private OriginFileService originFileService;
    @Autowired
    private CloudService cloudService;
//    @Autowired
//    private LogService logService;

    @RequestMapping(value = "/queryAll")
    public String queryAllOriginFileByType(@RequestParam String type, HttpSession session, HttpServletRequest httpServletRequest) {
        User user = getUserFromSession(session);
        if (user == null) {return "";}
        int pageNum=Integer.parseInt(httpServletRequest.getParameter("pageNum"));
        String result=JSON.toJSONString(originFileService.queryAllOriginFile(type ,user,pageNum));
        session.setAttribute("pageNum",pageNum);
        return result;
    }

    @RequestMapping(value = "/status/upload", method = RequestMethod.GET)
    public String queryUploadStatus(HttpSession session) {
        User user = getUserFromSession(session);
        /*
         * message：
         *   	 key			|	 value
         *   originFileName     |   (String)
         *      status		    |	(String)
         *     cloudList		|	(List<Map>)
         *    totalNumber		|	(int)
         *    transedNumber		|	(int)
         *      speed	     	|	(double)
         *    speedUnit			|	(String)
         *   -----------------------------------
         *   cloudList中Map的结构
         *   		key			|	 value
         *   	cloudName		|	(String)
         *  	upedNumber		|	 (int)
         *  	totalNumber		|	 (int)
         * 		   speed		|	(float)
         * 		speedUnit		|	(String)
         *
         */
        // messages表示传输到前端的信息,list中的每个元素都是一个正在上传的文件
        List<Map<String, Object>> messages = new ArrayList<>();
        // 上传映射表
//        Map<String, Map<String, Object>> uploadMap = FileManager.getUploadMap();
        System.out.println(JSON.toJSONString(user));
        System.out.println(JSON.toJSONString(session.getId()));
        Map<String, Map<String, Object>> uploadMap = FileManager.getUploadMapByUser(user);
        // 存入list中的元素
        Map<String, Object> element = null;
        for (Map.Entry<String, Map<String, Object>> entry : uploadMap.entrySet()) {
            element = new HashMap<>();
            element.put("FMID", entry.getKey());
            element.put("originFileName", entry.getValue().get(Constant.FILE_MANAGER_ORIGINFILENAME));
            element.put("status", entry.getValue().get(Constant.FILE_MANAGER_STATUS));
            element.put("code", entry.getValue().get(Constant.FILE_MANAGER_CODE));
            element.put("totalNumber", entry.getValue().get(Constant.FILE_MANAGER_TOTALNUMBER));
            element.put("transedNumber", entry.getValue().get(Constant.FILE_MANAGER_TRANSEDNUMBER));
            element.put("speed", entry.getValue().get(Constant.FILE_MANAGER_SPEED));
            element.put("speedUnit", entry.getValue().get(Constant.FILE_MANAGER_SPEEDUNIT));
            element.put("progress", entry.getValue().get(Constant.FILE_MANAGER_PROGRESS));
            element.put("cloudList", entry.getValue().get(Constant.FILE_MANAGER_CLOUDTASKS));
            messages.add(element);
        }


        return JSON.toJSONString(messages);
        //TODO 以下是测试数据，上线的时候，需要撤去
//        return getTempDatas();
    }

    private String getTempDatas() {
        String temp = "[{\"totalNumber\":0,\"transedNumber\":0,\"speedUnit\":\"KB/s\",\"originFileName\":\"Spark快速大数据分析.pdf\",\"progress\":0,\"speed\":0,\"status\":\"UP_WAIT\",\"code\":\"101\"},{\"totalNumber\":0,\"transedNumber\":0,\"speedUnit\":\"KB/s\",\"originFileName\":\"第三章翻译3.11(copy).doc\",\"progress\":0,\"speed\":0,\"status\":\"UP_WAIT\",\"code\":\"101\"},{\"totalNumber\":5,\"transedNumber\":5,\"speedUnit\":\"KB/s\",\"originFileName\":\"Screenshot from 2019-01-28 16-55-37.png\",\"cloudList\":[{\"totalNumber\":2,\"transedNumber\":2,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":14.12,\"cloudName\":\"tengxun\",\"speed\":11.55},{\"totalNumber\":2,\"transedNumber\":2,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":16.23,\"cloudName\":\"jinshan\",\"speed\":9.88},{\"totalNumber\":1,\"transedNumber\":1,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":13.42,\"cloudName\":\"baidu\",\"speed\":13.42}],\"progress\":\"100%\",\"speed\":34.85,\"status\":\"UPING\",\"code\":\"102\"},{\"totalNumber\":5,\"transedNumber\":5,\"speedUnit\":\"KB/s\",\"originFileName\":\"第三章-序言(copy).docx\",\"cloudList\":[{\"totalNumber\":2,\"transedNumber\":2,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":29.83,\"cloudName\":\"tengxun\",\"speed\":20.41},{\"totalNumber\":2,\"transedNumber\":2,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":23.24,\"cloudName\":\"jinshan\",\"speed\":14.94},{\"totalNumber\":1,\"transedNumber\":1,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":27.16,\"cloudName\":\"baidu\",\"speed\":27.16}],\"progress\":\"100%\",\"speed\":62.51,\"status\":\"UPING\",\"code\":\"102\"},{\"totalNumber\":5,\"transedNumber\":5,\"speedUnit\":\"KB/s\",\"originFileName\":\"Screenshot from 2019-01-28 16-54-21.png\",\"cloudList\":[{\"totalNumber\":2,\"transedNumber\":2,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":53.4,\"cloudName\":\"tengxun\",\"speed\":30.77},{\"totalNumber\":2,\"transedNumber\":2,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":6.78,\"cloudName\":\"jinshan\",\"speed\":6.47},{\"totalNumber\":1,\"transedNumber\":1,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":29.14,\"cloudName\":\"baidu\",\"speed\":29.14}],\"progress\":\"100%\",\"speed\":66.38,\"status\":\"UPING\",\"code\":\"102\"},{\"totalNumber\":10,\"transedNumber\":10,\"speedUnit\":\"KB/s\",\"originFileName\":\"Python编程：从入门到实践.pdf\",\"cloudList\":[{\"totalNumber\":3,\"transedNumber\":3,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":632.5,\"cloudName\":\"tengxun\",\"speed\":278.23},{\"totalNumber\":3,\"transedNumber\":3,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":805.28,\"cloudName\":\"jinshan\",\"speed\":221.39},{\"totalNumber\":3,\"transedNumber\":3,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":354.83,\"cloudName\":\"baidu\",\"speed\":173.86},{\"totalNumber\":1,\"transedNumber\":1,\"speedUnit\":\"KB/s\",\"progress\":\"100%\",\"maxSpeed\":405.35,\"cloudName\":\"qiniu\",\"speed\":405.35}],\"progress\":\"100%\",\"speed\":1078.83,\"status\":\"UPED\",\"code\":\"103\"}]";
        return temp;
    }

    @RequestMapping(value = "/status/download", method = RequestMethod.GET)
    public String queryDownloadStatus(HttpSession session) {
        User user = getUserFromSession(session);
        /*
         * message：
         *   	 key			|	 value
         *   originFileName     |   (String)
         *      status		    |	(String)
         *     cloudList		|	(List<Map>)
         *    totalNumber		|	(int)
         *    transedNumber		|	(int)
         *      speed	     	|	(double)
         *    speedUnit			|	(String)
         *   -----------------------------------
         *   cloudList中Map的结构
         *   		key			|	 value
         *   	cloudName		|	(String)
         *  	upedNumber		|	 (int)
         *  	totalNumber		|	 (int)
         * 		   speed		|	(float)
         * 		speedUnit		|	(String)
         *
         */
        // messages表示传输到前端的信息,list中的每个元素都是一个正在下载的文件
        List<Map<String, Object>> messages = new ArrayList<>();
        // 上传映射表
//        Map<String, Map<String, Object>> downloadMap = FileManager.getDownloadMap();
        Map<String, Map<String, Object>> downloadMap = FileManager.getDownloadMapByUser(user);
        // 存入list中的元素
        Map<String, Object> element = null;
        for (Map.Entry<String, Map<String, Object>> entry : downloadMap.entrySet()) {
            element = new HashMap<>();
            element.put("FMID", entry.getKey());
            element.put("originFileName", entry.getValue().get(Constant.FILE_MANAGER_ORIGINFILENAME));
            element.put("status", entry.getValue().get(Constant.FILE_MANAGER_STATUS));
            element.put("totalNumber", entry.getValue().get(Constant.FILE_MANAGER_TOTALNUMBER));
            element.put("transedNumber", entry.getValue().get(Constant.FILE_MANAGER_TRANSEDNUMBER));
            element.put("speed", entry.getValue().get(Constant.FILE_MANAGER_SPEED));
            element.put("speedUnit", entry.getValue().get(Constant.FILE_MANAGER_SPEEDUNIT));
            element.put("progress", entry.getValue().get(Constant.FILE_MANAGER_PROGRESS));
            element.put("cloudList", entry.getValue().get(Constant.FILE_MANAGER_CLOUDTASKS));
            messages.add(element);
        }

        return JSON.toJSONString(messages);

    }

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public String getHistoryLogs(HttpSession session) {
        User user = getUserFromSession(session);
        HistoryLogService historyLogService = new HistoryLogService(user);
        List<Log> histories = historyLogService.getAllHistories();
        return JSON.toJSONString(histories);
    }

//    @RequestMapping(value = "/status/all", method = RequestMethod.GET)
//    public String queryAllStatus() {
//
//        /*
//         * message：
//         *   	 key			|	 value
//         *   originFileName     |   (String)
//         *      status		    |	(String)
//         *     cloudList		|	(List<Map>)
//         *    totalNumber		|	(int)
//         *    transedNumber		|	(int)
//         *      speed	     	|	(double)
//         *    speedUnit			|	(String)
//         *   -----------------------------------
//         *   cloudList中Map的结构
//         *   		key			|	 value
//         *   	cloudName		|	(String)
//         *  	upedNumber		|	 (int)
//         *  	totalNumber		|	 (int)
//         * 		   speed		|	(float)
//         * 		speedUnit		|	(String)
//         *
//         */
//        // messages表示传输到前端的信息,list中的每个元素都是一个正在下载的文件
//        List<Map<String, Object>> messages = new ArrayList<>();
//        // 上传映射表
//        Map<String, Map<String, Object>> allMap = FileManager.getAllMap();
//        // 存入list中的元素
//        Map<String, Object> element = null;
//        for (Map.Entry<String, Map<String, Object>> entry : allMap.entrySet()) {
//            element = new HashMap<>();
//            element.put("originFileName", entry.getKey());
//            element.put("status", entry.getValue().get(Constant.FILE_MANAGER_STATUS));
//            element.put("totalNumber", entry.getValue().get(Constant.FILE_MANAGER_TOTALNUMBER));
//            element.put("transedNumber", entry.getValue().get(Constant.FILE_MANAGER_TRANSEDNUMBER));
//            element.put("speed", entry.getValue().get(Constant.FILE_MANAGER_SPEED));
//            element.put("speedUnit", entry.getValue().get(Constant.FILE_MANAGER_SPEEDUNIT));
//            element.put("progress", entry.getValue().get(Constant.FILE_MANAGER_PROGRESS));
//            element.put("cloudList", entry.getValue().get(Constant.FILE_MANAGER_CLOUDTASKS));
//            messages.add(element);
//        }
//
//        return JSON.toJSONString(messages);
//    }

//    /**
//     * 下载 云->HSB
//     * data的格式为
//     * {
//     * {id1}:{name}
//     * {id2}:{name}
//     * .
//     * .
//     * .
//     * }
//     */
//    @RequestMapping(value = "/download", method = RequestMethod.POST)
//    public void download(@RequestBody String data) {
//        // 获取body中传输过来的json格式的data数据
//        // "id"--"name"
//        Map<String, Object> downloadFiles = JSON.parseObject(data);
//        // 利用线程池启动下载线程
//        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
//        // 对该次批量下载进行一个下载初始化
//        for (Object file : downloadFiles.values()) {
//            // 初始化的同时，赋予了200code
//            FileManager.initDownloadCloudTasks(String.valueOf(file));
//        }
//        // 没有在上一个for循环中直接运行文件下载机线程的原因是：
//        // 后期可能会由于内存溢出等原因，暂停线程的start，而是通过run方法直接运行，
//        // 如果是在上述循环中，暂停后，用户必须等前一个文件下载好，才可以初始化下一个云下载任务列表，给用户的视觉体验不好，显得程序很慢
//        for (Map.Entry<String, Object> entry : downloadFiles.entrySet()) {
//            // 取线程池中的一个线程进行下载和拼装
//            fixedThreadPool.execute(new FileDownload(
//                    Integer.valueOf(entry.getKey()),   // 取源文件ID
//                    String.valueOf(entry.getValue())   // 取源文件名字
//            ));
//        }
//    }

    private User getUserFromSession(HttpSession session) {
        if (session.getAttribute("openid") == null) {
            return null;
        }
        return new User(session.getAttribute("openid").toString());
    }
}
