package com.uestc.piecescloud.conventions.strategy.upload;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.service.database.storage.CloudService;
import com.uestc.piecescloud.utils.ApplicationContextUtil;

import java.util.*;

/**
 * @Description: 平均策略。即将所有的碎片文件均匀上传至各个可用的云上$
 * @Author: 红雨澜山梦
 * @create: 2019-02-28 21:39
 */
public class Average {

    private List<FilePart> fileParts;
    // 初始的云集合
    private Set<String> clouds;

    public Average(List<FilePart> fileParts) {
        this.fileParts = fileParts;
        this.clouds = new HashSet<>();
    }

    public Average(List<FilePart> fileParts, Set<CLOUD> clouds) {
        this(fileParts);
        this.clouds = CLOUD.convertToSet(clouds);
    }


    /**
     * 目前是根据均等切分进行的上传策略生成
     *
     * @return
     */
    public Map<CLOUD, List<FilePart>> generate() {
        // 选择云,将云存入成员变量的clouds中
        selectCLouds();
        // 根据成员变量的clouds,选择任务
        Map<CLOUD, List<FilePart>> tasks = randomSelectTask();

        return tasks;

    }

    /**
     * 根据构造器中初始化的成员变量集合,和目前数据库可用的云集合取交集,得到最终的云列表.若此时云列表为空,则云列表为可用云的列表
     */
    private void selectCLouds() {
        // 可用的云情况
        Set<String> cloudsWorkable = new HashSet<String>(ApplicationContextUtil.getBean(CloudService.class).queryAllWorkable());
        // TODO 若无云可用
        if (cloudsWorkable.size() == 0 || cloudsWorkable == null) {
            // ...
        }
        // 取交集
        this.clouds.retainAll(cloudsWorkable);
        // 若交集后,无元素,则取可用云
        if (this.clouds == null || this.clouds.size() == 0) {
            this.clouds = cloudsWorkable;
        }
    }

    /**
     * 自动分配云所需要的上传的文件碎片
     *
     * @return
     */
    private Map<CLOUD, List<FilePart>> randomSelectTask() {
        Map<CLOUD, List<FilePart>> cloudTasks = new HashMap<>();
        // 初始化云任务列表
        for (String cloud : this.clouds) {
            cloudTasks.put(CLOUD.valueOf(cloud.toUpperCase()), new ArrayList<FilePart>());
        }

        // 计算每个云的平均上传的碎片数量
        int averageCount = (int) Math.ceil(
                (double) this.fileParts.size() / (double) this.clouds.size());
        // 打乱碎片文件列表
        Collections.shuffle(this.fileParts);
        // 向每个云插入任务碎片文件
        int index = 0;
        for (Map.Entry<CLOUD, List<FilePart>> entry : cloudTasks.entrySet()) {
            for (int i = 0; i < averageCount; i++) {
                // 若index大于等于碎片文件的大小，则跳出内层循环
                if (index >= this.fileParts.size()) {
                    break;
                }
                // 在云任务中的相应云下按照乱序后的顺序添加碎片文件
                entry.getValue().add(this.fileParts.get(index++));
            }

        }
//        System.out.println(JSON.toJSONString(cloudTasks));
        return cloudTasks;
    }



}



















