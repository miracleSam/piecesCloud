package com.uestc.piecescloud.conventions.strategy;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.conventions.strategy.filesplit.FilePartSize;
import com.uestc.piecescloud.conventions.strategy.upload.Average;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @Description: 根据待上传文件的属性，选择合适的上传策略$
 * @Author: 红雨澜山梦
 * @create: 2019-02-28 21:49
 */
@Component
public class AutoChooseSuitStrategy {

    /**
     * 获取按照文件大小进行的文件切分的策略
     *
     * @param length 源文件
     * @return 碎片文件的大小
     */
    public static int getFileSplitStrategy(long length) {
        return FilePartSize.suitByteSize(length);
    }

    /**
     * 获取文件上传策略
     *
     * @param fileParts 碎片文件上传列表
     * @return 上传策略
     */
    public static Map<CLOUD, List<FilePart>> getFileUploadStrategy(List<FilePart> fileParts) {

        return new Average(fileParts).generate();
    }

}
