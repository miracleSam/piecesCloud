package com.uestc.piecescloud.conventions.strategy.filesplit;

/**
 * @Description: 文件切片的时候，文件碎片大小的切片约定$
 * @Author: 红雨澜山梦
 * @create: 2019-02-24 16:53
 */
public class FilePartSize {

    /**
     * 根据源文件的文件大小，确定文件碎片的大小
     *
     * @param length 源文件的文件大小
     * @return 文件碎片的大小
     */
    public final static int suitByteSize(long length) {

        int byteSize;

        if (length > 0 && length < 1024 * 1024) { //小于1M
            byteSize = (int) Math.ceil((double) length / (5.0 * 1024)) * 1024;  //5个碎片
        } else if (length <= 100 * 1024 * 1024 && length >= 1024 * 1024) {  //小于100M 大于1M
            byteSize = (int) Math.ceil((double) length / (10.0 * 1024)) * 1024;  //10个碎片
        } else if (length > 100 * 1024 * 1024 && length <= 1024 * 1024 * 1024) { //大于100M且小于1G
            byteSize = (int) Math.ceil((double) length / (50.0 * 1024)) * 1024;  //50个碎片
        } else if (length > 1024 * 1024 * 1024) {  //大于1G
            byteSize = (int) Math.ceil((double) length / (100.0 * 1024)) * 1024; //100个碎片
        } else {
            byteSize = 5 * 1024 * 1024;
        }
        return byteSize;
    }
}
