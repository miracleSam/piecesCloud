package com.uestc.piecescloud.dao;

import com.uestc.piecescloud.bean.FilePart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 一级索引数据持久化层方法$
 * @Author: 红雨澜山梦
 * @create: 2019-03-22 20:30
 */
@Mapper
public interface Lv1IndexDao {
    void insert(@Param("originFileId") int originFileId, @Param("fileParts") List<FilePart> fileParts);

}
