package com.uestc.piecescloud.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: 持久化层对数据表云的操作$
 * @Author: 红雨澜山梦
 * @create: 2019-03-04 20:19
 */
@Mapper
public interface CloudDao {

    List<String> queryAll();

    List<String> queryAllWorkable();

    int queryIdByName(String name);


}
