package com.uestc.piecescloud.dao;

import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.File;
import java.util.List;

/**
 * @Description: 关于用户操作的源文件的数据库持久化层$
 * @Author: 红雨澜山梦
 * @create: 2019-02-01 21:25
 */
@Mapper
public interface OriginFileDao {

    List<OriginFile> queryAll(@Param("types") String[] types, @Param("user") User user);

    int insertOne(OriginFile originFile);

    int deleteOne();


}
