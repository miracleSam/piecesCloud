package com.uestc.piecescloud.dao;

import com.uestc.piecescloud.bean.FilePart;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Description: 文件碎片的数据持久化操作$
 * @Author: 红雨澜山梦
 * @create: 2019-03-22 19:24
 */
@Mapper
public interface FilePartDao {

//    int insertMany(List<FilePart> fileParts);

    // 由于insertMany无法批量返回主键值,故用事务管理进行循环插入
    int insertOne(FilePart filePart);

    /**
     * 根据originFileId查询云任务列表
     *
     * @param originFileId 源文件ID
     */
    List<FilePart> queryFilePartsByOriginFileId(int originFileId);

    /**
     * 查询指定的源文件ID所对应的碎片数量信息
     *
     * @param cloudName    云名字
     * @param originFileId 源文件ID
     * @return map结构如下：（所有字段，均为可选字段）
     * “baidu”  :  int
     * “qiniu”  :  int
     * “jinshan”  :  int
     * “tengxun”  :  int
     */
    int getFilePartsCount(@Param("cloudName") String cloudName, @Param("originFileId") int originFileId);
}
