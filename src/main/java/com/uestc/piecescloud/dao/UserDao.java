package com.uestc.piecescloud.dao;

import com.sun.corba.se.spi.ior.ObjectKey;
import com.uestc.piecescloud.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface UserDao {

    User hadUser(User user);

    void register(User user);

    void modifyUserInfo(User user);

    void addFriend(int userId1, int userId2);

    List<HashMap<String, Object>> showFriends(User user);

    List<User> queryUsersByIds(List<Integer> ids);

    void addGet(int userId, int number);

    void addPut(int userId, int number);

//    void showGetPut(int userId);

    void changeUsedSpace(User user);

    void changeTotalSpace(User user);

    void insertUserOriginFile(@Param("userId") int userId, @Param("originFileId") int originFileId);
}



