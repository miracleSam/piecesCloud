package com.uestc.piecescloud.dao;

import com.uestc.piecescloud.bean.FilePart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description: 二级索引数据持久化层方法$
 * @Author: 红雨澜山梦
 * @create: 2019-03-22 20:31
 */
@Mapper
public interface Lv2IndexDao {
    void insert(@Param("cloudId") int cloudId, @Param("fileParts") List<FilePart> fileParts);
}
