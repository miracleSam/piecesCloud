package com.uestc.piecescloud.dao;

import com.uestc.piecescloud.bean.Log;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Description: 日志持久化层$
 * @Author: 红雨澜山梦
 * @create: 2019-03-28 17:19
 */
@Deprecated
@Mapper
public interface LogDao {

    /**
     * 插入一条源文件内存信息的日志记录
     *
     * @param originFileInfoInMemory 源文件内存信息
     */
    void insertOne(Map<String, Object> originFileInfoInMemory);

    /**
     * 查询所有已经完成的源文件内存信息的日志记录
     */
    List<Log> queryAllFinished();
}
