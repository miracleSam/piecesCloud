package com.uestc.piecescloud.service.user;

import com.sun.tools.internal.jxc.ap.Const;
import com.uestc.piecescloud.bean.Log;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.CLOUD;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 生成或者获取各个用户的配置文件，管理每个用户的个性化设置
 * @Author: 红雨澜山梦
 * @Date: 2019/7/2 10:20
 * @Version: 0.0.1
 */
public class UserConfiguration {

    /*
     * 用户的配置文件命名格式为 openid.xml 默认MAC地址为gggggg
     */
    public static void initial(User user) {

        String openidDir = Constant.HSB_USER + File.separator + user.getOpenid();
        // step1：在users目录下新建{openid}的目录
        new File(openidDir).mkdirs();
        // step2：在step1目录下新建upload download tmp_view文件夹
        new File(openidDir+File.separator+Constant.HSB_USER_UPLOAD).mkdir();
        new File(openidDir+File.separator+Constant.HSB_USER_DOWNLOAD).mkdir();
        new File(openidDir+File.separator+Constant.HSB_USER_TMP_VIEW).mkdir();
        new File(openidDir+File.separator+Constant.HSB_USER_TMP_VIEW+Constant.HSB_USER_TMP_VIEW_ALL).mkdir();
        new File(openidDir+File.separator+Constant.HSB_USER_TMP_VIEW+Constant.HSB_USER_TMP_VIEW_UP).mkdir();
        new File(openidDir+File.separator+Constant.HSB_USER_TMP_VIEW+Constant.HSB_USER_TMP_VIEW_DOWN).mkdir();
        // step3：新建setting.xml文件
        try{
            initialUserSetting(openidDir);
            initialUserHistory(openidDir);
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }


    private static void initialUserSetting(String path) throws IOException {
        File file = new File(path + File.separator + Constant.HSB_USER_SETTING);
        if (file.exists()) {return;}
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("root");
        Element cloud = root.addElement("cloud");
        List<String> cloudnames = CLOUD.list();
        for (String cloudname : cloudnames) {
            // 添加cloud里面的 各个云提供商的名字
            Element e = cloud.addElement(cloudname);
            e.addAttribute("isUsed", "true");
        }

        Element trans = root.addElement("trans");
        trans.addElement("maxUpload").setText("1");
        trans.addElement("maxDownload").setText("1");
        trans.addElement("isRealSplit").setText("true");
        trans.addElement("isRealMerge").setText("true");


        // 5、设置生成xml的格式
        OutputFormat format = OutputFormat.createPrettyPrint();
        // 设置编码格式
        format.setEncoding("UTF-8");


        // 6、生成xml文件
        XMLWriter writer = new XMLWriter(new FileOutputStream(file), format);
        // 设置是否转义，默认使用转义字符
        writer.setEscapeText(false);
        writer.write(document);
        writer.close();
        System.out.println("生成"+file.getAbsolutePath()+"成功");


    }

    public static void initialUserHistory(String path) {
        File file = new File(path + File.separator + Constant.HSB_USER_HISTORY);
        if (file.exists()) {return;}
        String initialInfo = "originFileId\ttimestamp\tfilename\tstatus_code\tstatus\tupload\tdownload";
        BufferedWriter out = null;
        try {
            file.createNewFile();
            byte[] bytes = new byte[512];
            bytes = initialInfo.getBytes();
            int b = bytes.length;
            out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file), "UTF-8"));
            out.write(initialInfo);
            out.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                    System.out.println("生成history.log成功");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void main(String[] args) {
//        User user = new User();
//        user.setOpenid("usersksksks");
//        initial(user);
//        System.out.println(getIsRealSplit(user));
        initial(new User("testForHistory"));
    }


    public static boolean getIsRealSplit(User user) {
        String openid = user.getOpenid();
        Document settings = getSettingsXML(openid);
        Element root = settings.getRootElement();
        boolean isRealSplit = Boolean.valueOf(
                root.element("trans").element("isRealSplit").getText());
        return isRealSplit;
    }

    public static boolean getIsRealMerge(User user) {
        String openid = user.getOpenid();
        Document settings = getSettingsXML(openid);
        Element root = settings.getRootElement();
        boolean isRealMerge = Boolean.valueOf(
                root.element("trans").element("isRealMerge").getText());
        return isRealMerge;
    }

    private static Document getSettingsXML(String openid) {
        SAXReader reader = new SAXReader();
        Document settings = null;
        try {
            settings = reader.read(new File(Constant.HSB_USER + File.separator +openid + File.separator + Constant.HSB_USER_SETTING));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return settings;
    }

}
