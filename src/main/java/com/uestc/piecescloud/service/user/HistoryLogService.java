package com.uestc.piecescloud.service.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.uestc.piecescloud.bean.Log;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: 红雨澜山梦
 * @Date: 2019-07-18 15:56
 */

public class HistoryLogService {

    private static String regex = "\t";

    private User user;
    private String openid;

    public HistoryLogService(@NotNull User user) {
        this.user = user;
        this.openid = user.getOpenid();
    }

    public List<Log> getAllHistories() {
        File file = new File(Constant.HSB_USER + File.separator +
                openid + File.separator + Constant.HSB_USER_HISTORY);
        List<Log> histories = new ArrayList<>();
        try {
            InputStreamReader inputReader = new InputStreamReader(new FileInputStream(file));
            BufferedReader bf = new BufferedReader(inputReader);
            String logStr ;
            // 取出第一行的字段名称，因为程序正常的时候，第一行一定是有数据的
            String[] field = bf.readLine().split(regex);
            JSONObject logJSONObject;
            while((logStr = bf.readLine()) != null) {
                logJSONObject = new JSONObject();
                String[] logInfos = logStr.split(regex);
                // 为
                for (int index=0; index<logInfos.length; index++) {
                    logJSONObject.put(field[index], logInfos[index]);
                }
                histories.add(
                        JSON.parseObject(logJSONObject.toJSONString(), Log.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return histories;
    }

    /**
     * 于history.log文件尾部添加文件历史记录
     * @param log
     */
    public void insertOneLog(Log log) {
        String logStr = log.generateLineInfo(regex);
        File file = new File(Constant.HSB_USER + File.separator +
                openid + File.separator + Constant.HSB_USER_HISTORY);
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file, true), "UTF-8"));
            out.write(logStr);
            out.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    public static void main(String[] args) {
//        Log log = new Log();
//        log.setTimestamp(System.currentTimeMillis());
//        log.setFileName("特殊1.txt");
//        log.setStatus("OK");
//        log.setStatus_code("129");
//        log.setUpload(true);
//        new HistoryLogService(new User("testForHistory")).insertOneLog(log);
////        log.setDownload();
//        System.out.println(
//                new HistoryLogService(new User("testForHistory")).getAllHistories()
//        );
//
//        Map<String, Object> map = new HashMap<>();
//        map.put("fileName","test.txt");
//        map.put("timestamp",System.currentTimeMillis());
//        map.put("status","OK");
//        map.put("status_code", "411");
//        map.put("upload", true);
//        Log log2 = JSON.parseObject(JSON.toJSONString(map), Log.class);
//        new HistoryLogService(new User("testForHistory")).insertOneLog(log2);
////        log.setDownload();
//        System.out.println(
//                new HistoryLogService(new User("testForHistory")).getAllHistories()
//        );
//    }

}
