package com.uestc.piecescloud.service.cloud;

import java.util.ResourceBundle;

/**
 * 为了避免由于用户通行证的问题，降低云工具类与用户通行证的耦合度问题。
 * 这里统一使用get,set方法进行用户通行证的设置和获取。
 * @Author: 红雨澜山梦
 * @Date: 2019/2/23 22:39
 * @Version: 0.0.1
 */
public class CloudUserConfiguration {

    private static ResourceBundle baiduBundle = ResourceBundle.getBundle("baidu_cloud");
    private static ResourceBundle tengxunBundle = ResourceBundle.getBundle("tengxun_cloud");
    private static ResourceBundle qiniuBundle = ResourceBundle.getBundle("qiniu_cloud");
    private static ResourceBundle jinshanBundle = ResourceBundle.getBundle("jinshan_cloud");

    public static String getBaiduAKI() {
        return baiduBundle.getString("ACCESS_KEY_ID");
    }

    public static String getBaiduSAK() {
        return baiduBundle.getString("SECRET_ACCESS_KEY");
    }

    public static String getBaiduEP() {
        return baiduBundle.getString("ENDPOINT");
    }

    public static String getBaiduBucketName() {
        return baiduBundle.getString("bucketName");
    }

    public static String getTengxunAKI() {
        return tengxunBundle.getString("ACCESS_KEY_ID");
    }

    public static String getTengxunSAK() {
        return tengxunBundle.getString("SECRET_ACCESS_KEY");
    }

    public static String getTengxunEP() {
        return tengxunBundle.getString("ENDPOINT");
    }

    public static String getTengxunBucketName() {
        return tengxunBundle.getString("bucketName");
    }

    public static String getQiniuAKI() {
        return qiniuBundle.getString("ACCESS_KEY_ID");
    }

    public static String getQiniuSAK() {
        return qiniuBundle.getString("SECRET_ACCESS_KEY");
    }

    public static String getQiniuDomain() {
        return qiniuBundle.getString("domainOfBucket");
    }

    public static String getQiniuBucketName() {
        return qiniuBundle.getString("bucketName");
    }

    public static String getJinshanAKI() {
        return jinshanBundle.getString("ACCESS_KEY_ID");
    }

    public static String getJinshanSAK() {
        return jinshanBundle.getString("SECRET_ACCESS_KEY");
    }

    public static String getJinshanDomain() {
        return jinshanBundle.getString("domainOfBucket");
    }

    public static String getJinshanBucketName() {
        return jinshanBundle.getString("bucketName");
    }

}
