package com.uestc.piecescloud.service.cloud;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Description: 调用云提供商所提供的方法（读操作），通过适配器的模式进行调用，此接口主要为所需要的方法的集合$
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 23:14
 */
public interface IRead extends Runnable {

    /**
     * 初始化，实际上就是实例化一个client
     */
    void init();

    void startDown(OriginFile originFile, List<FilePart> tasks, CountDownLatch countDownLatch);

    void simpleDownload_r(FilePart filePart);

    void simpleDownload_v(FilePart filePart);

    void resumeBrokenDownload_r(FilePart filePart);

    void resumeBrokenDownload_v(FilePart filePart);

    void query();

}
