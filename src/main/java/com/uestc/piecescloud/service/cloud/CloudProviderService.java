package com.uestc.piecescloud.service.cloud;

import com.uestc.piecescloud.constant.enums.CLOUD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: cloud包对外的接口
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 21:14
 */
@Service
public class CloudProviderService {

    /**
     * 获根据cloud,得一个相应云平台写操作的实例.
     *
     * @param cloud 云
     * @return 写操作实例, 用于上传, 修改, 删除等操作
     */
    public IWrite newWriteInstance(CLOUD cloud) {
        return CloudFactory.newWriteInstance(cloud);
    }

    /**
     * 获根据cloud,得一个相应云平台读操作的实例.
     *
     * @param cloud 云
     * @return 写操作实例, 用于下载，预览，查找等操作
     */
    public IRead newReadInstance(CLOUD cloud) {
        return CloudFactory.newReadInstance(cloud);
    }

}
