package com.uestc.piecescloud.service.cloud.qiniu;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.service.cloud.CloudUserConfiguration;
import com.uestc.piecescloud.service.cloud.IRead;
import com.uestc.piecescloud.service.cloud.baidu.BaiduUtil;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.utils.Calculate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @Description: 利用适配器模式，使云提供商所提供的方法，更好的契合项目$
 * @Author: 红雨澜山梦
 * @Date: 2019/3/30 0:54
 * @Version: 0.0.1
 */
public class QiniuReadAdapter implements IRead {

    private QiniuUtil qiniuUtil;
    private String bucketName;
    private String domainBucket;

    private static final String cloudName = QiniuUtil.CLOUD_NAME;
    private List<FilePart> tasks;
    private OriginFile originFile;

    private CountDownLatch countDownLatch;

    @Override
    public void init() {
        if (qiniuUtil != null)
            return;
        String AKI = CloudUserConfiguration.getQiniuAKI();
        String SAK = CloudUserConfiguration.getQiniuSAK();
        this.domainBucket = CloudUserConfiguration.getQiniuDomain();
        this.bucketName = CloudUserConfiguration.getQiniuBucketName();
        qiniuUtil = new QiniuUtil(AKI, SAK, bucketName);
    }

    @Override
    public void simpleDownload_r(FilePart filePart) {
        init();
        qiniuUtil.simpleDownloadToFile(filePart.getName(), domainBucket, filePart.getAbsolutePath());
    }

    @Override
    public void simpleDownload_v(FilePart filePart) {
        // TODO 目前对于文件下载的追加拼接并没有明确的思路，此处暂时调用简单下载的实际文件的下载
        simpleDownload_r(filePart);
    }

    @Override
    public void resumeBrokenDownload_r(FilePart filePart) {

    }

    @Override
    public void resumeBrokenDownload_v(FilePart filePart) {

    }

    @Override
    public void query() {

    }

    @Override
    public void run() {
        // 取源文件对象
        String FMID = originFile.getFMID();
        // 目前 恒为false
        boolean isAppendMerge = originFile.isAppendMerge();

        // 根据源文件名和云名字初始化云任务列表,有则直接取索引,无则初始化新的地址空间
        Map<String, Object> cloudTask = FileManager.initCloudTask(FMID, cloudName);
//        "cloudName"： String
//        "transedNumber" ： int
//        "totalNumber" ： int
//        "speed" ： double (默认是KB/S)
//        "speedUnit" ： String ("MB/s KB/s B/s")
//        "maxSpeed" : double
        // 存入总的任务量
        cloudTask.put(Constant.FILE_MANAGER_TOTALNUMBER, tasks.size());
        // 记录最高速度,默认是KB/s
        double maxSpeed = 0.0;
        // 记录单个文件平均速度
        double speed = 0.0;
        // 已下载的数量
        int downedNumber = 0;
        // 计算平均速度
        long start = System.currentTimeMillis();
        for (FilePart filePart : tasks) {
            filePart.setParent(originFile.getParent());
            // 计算单个文件上传速度,主要用于记录最高速度
            long s = System.currentTimeMillis();

            // 启动下载方法
            if (isAppendMerge)
                // 普通拼装（实拼）中的下载
                simpleDownload_r(filePart);
            else
                // 追加拼装（虚拼）中的下载
                // TODO 由于追加拼装后需要立即执行文件合并机，所以此处下载的逻辑有可能会更改
                simpleDownload_v(filePart);

            // 计算单个文件碎片的结束时间,如果是最后一个文件,也记录整个文件的结束时间
            long end = System.currentTimeMillis();

            // 平均速度默认以KB/s为单位
            speed = Calculate.getSpeed(start, end, filePart.getLength(),
                    cloudTask.get(Constant.FILE_MANAGER_SPEEDUNIT).toString());
            cloudTask.put(Constant.FILE_MANAGER_SPEED, speed);
            cloudTask.put(Constant.FILE_MANAGER_TRANSEDNUMBER, ++downedNumber);
            cloudTask.put(Constant.FILE_MANAGER_PROGRESS,
                    Calculate.doubleToPercent(
                            new BigDecimal((float) downedNumber / ((float) tasks.size())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
            if (speed > maxSpeed) {
                maxSpeed = speed;
                cloudTask.put(Constant.FILE_MANAGER_MAX_SPEED, maxSpeed);
            }

        }
        // 线程同步 -1
        countDownLatch.countDown();
    }

    @Override
    public void startDown(OriginFile originFile, List<FilePart> tasks, CountDownLatch countDownLatch) {
        Thread thread = new Thread(this);
        this.tasks = tasks;
        this.originFile = originFile;
        this.countDownLatch = countDownLatch;
        thread.start();
    }
}
