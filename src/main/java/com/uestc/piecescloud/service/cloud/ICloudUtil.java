package com.uestc.piecescloud.service.cloud;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @Description: 本项目中，所需要实现的基础云提供商所提供的功能
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 21:22
 */
public interface ICloudUtil {

    /**
     * 文件简单上传
     * @param file
     * @param fileName
     * @param bucketName
     * @return 简单上传的执行情况
     */
    boolean simpleUpload_r(File file, String fileName, String bucketName);

    /**
     * 文件流简单上传
     * @param inputStream
     * @param fileName
     * @param bucketName
     * @return 简单上传的执行情况
     */
    boolean simpleUpload_v(InputStream inputStream, String fileName, String bucketName);

    /**
     * 断点续传_实切
     * @return 断点续传的执行情况
     */
    boolean resumeBrokenUpload_r(File file, String fileName, String bucketName);

    /**
     * 断点续传_虚切
     *
     * @return 断点续传的执行情况
     */
    boolean resumeBrokenUpload_v(InputStream inputStream, String fileName, String bucketName);

    String getFileUrl(String objectKey, String domainOfBucket) throws UnsupportedEncodingException;

    /**
     * 文件流简单下载
     *
     * @param objectKey  存于云中的对象名称
     * @param bucketName 云中的空间的名字
     * @param outputFileAbsolutePath 输出文件的绝对路径
     * @return 输入流的引用
     */
    InputStream simpleDownloadToInputStream(String objectKey, String bucketName, String outputFileAbsolutePath);

    /**
     * 文件简单下载
     *
     * @param objectKey  存于云中的对象名称
     * @param bucketName 云中的空间的名字
     * @param outputFileAbsolutePath 输出文件的绝对路径
     * @return 文件的引用
     */
    File simpleDownloadToFile(String objectKey, String bucketName, String outputFileAbsolutePath);

    /**
     * 云上单个文件删除
     *
     * @param objectKey
     * @param bucketName
     * @return
     */
    boolean deleteOne(String objectKey, String bucketName);

    /**
     * 云上多个文件删除
     *
     * @param objectKeys
     * @param bucketName
     * @return
     */
    boolean deleteMany(List<String> objectKeys, String bucketName);

}





