package com.uestc.piecescloud.service.cloud.ali;

import com.uestc.piecescloud.service.cloud.ICloudUtil;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @Description:
 * @Author: 红雨澜山梦
 * @Date: 2019-07-15 11:31
 */
public class AliUtil implements ICloudUtil{
    @Override
    public boolean simpleUpload_r(File file, String fileName, String bucketName) {
        return false;
    }

    @Override
    public boolean simpleUpload_v(InputStream inputStream, String fileName, String bucketName) {
        return false;
    }

    @Override
    public boolean resumeBrokenUpload_r(File file, String fileName, String bucketName) {
        return false;
    }

    @Override
    public boolean resumeBrokenUpload_v(InputStream inputStream, String fileName, String bucketName) {
        return false;
    }

    @Override
    public String getFileUrl(String objectKey, String domainOfBucket) throws UnsupportedEncodingException {
        return null;
    }

    @Override
    public InputStream simpleDownloadToInputStream(String objectKey, String bucketName, String outputFileAbsolutePath) {
        return null;
    }

    @Override
    public File simpleDownloadToFile(String objectKey, String bucketName, String outputFileAbsolutePath) {
        return null;
    }

    @Override
    public boolean deleteOne(String objectKey, String bucketName) {
        return false;
    }

    @Override
    public boolean deleteMany(List<String> objectKeys, String bucketName) {
        return false;
    }
}
