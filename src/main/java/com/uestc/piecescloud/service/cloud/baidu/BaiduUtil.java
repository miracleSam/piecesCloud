package com.uestc.piecescloud.service.cloud.baidu;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.bos.BosClientConfiguration;
import com.baidubce.services.bos.model.*;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.service.cloud.ICloudUtil;
import com.baidubce.services.bos.BosClient;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 实现部分所需的且百度云提供商所能提供的对外接口$
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 21:25
 */
public class BaiduUtil implements ICloudUtil {

    public final static String CLOUD_NAME = Constant.BAIDU;

    /*
     * 对象属性
     */
    private String ACCESS_KEY_ID = "";
    private String SECRET_ACCESS_KEY = "";
    private String ENDPOINT = "";
    private BosClient client = null;

    /*
     * 用于断点续传时，计算速度用
     */
    // 设置每块为 5MB
    // 此处设置为小于5MB（测试文件大小为10M）
    // 会报错：（com.baidubce.BceClientException: Unable to verify integrity of data upload.  Client calculated content hash didn't match hash calculated by BOS.  You may need to delete the data stored in BOS.）
    private final long partSize = 1024 * 1024 * 5L;
    private int partUploadCount = 0;

    /**
     * 获取断点续传中实切中的上传总量
     *
     * @return
     */
    public long getTotalUploadedResumeBrokenUpload_r() {
        return partUploadCount * partSize;
    }

    /**
     * 构造一个百度云工具对象，该对象在实例化的过程中，已经初始化了一个bosclient。
     *
     * @param ACCESS_KEY_ID
     * @param SECRET_ACCESS_KEY
     * @param ENDPOINT          域名。目前支持“华北-北京”、“华南-广州”和“华东-苏州”三个区域。北京区域：http://bj.bcebos.com，广州区域：http://gz.bcebos.com，苏州区域：http://su.bcebos.com。
     */
    public BaiduUtil(String ACCESS_KEY_ID, String SECRET_ACCESS_KEY, String ENDPOINT) {
        this.ACCESS_KEY_ID = ACCESS_KEY_ID;
        this.SECRET_ACCESS_KEY = SECRET_ACCESS_KEY;
        this.ENDPOINT = ENDPOINT;
        this.client = bosClientInit(ACCESS_KEY_ID, SECRET_ACCESS_KEY, ENDPOINT);
    }

    /**
     * BosClient初始化
     *
     * @return bosclient
     */
    private BosClient bosClientInit(String ACCESS_KEY_ID, String SECRET_ACCESS_KEY, String ENDPOINT) {
        // 初始化一个BosClient
        BosClientConfiguration config = new BosClientConfiguration();
        config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
        config.setEndpoint(ENDPOINT);
        // 设置HTTP最大连接数为10
        config.setMaxConnections(10);
        // 设置TCP连接超时为5000毫秒
        config.setConnectionTimeoutInMillis(5000);
        // 设置Socket传输数据超时的时间为20000毫秒
        config.setSocketTimeoutInMillis(20000);
        BosClient client = new BosClient(config);
        return client;
    }

    @Override
    public boolean simpleUpload_r(File file, String fileName, String bucketName) {
        boolean status = true;

        // 初始化上传输入流
        ObjectMetadata meta = new ObjectMetadata();
        // 设置ContentLength大小
        meta.setContentLength(1000);
        // 设置ContentType
        meta.setContentType("application/json");
        // 设置cache-control
        meta.setCacheControl("no-cache");
        // 设置x-bce-content-crc32
        meta.setxBceCrc("crc");
        // 设置自定义元数据name的值为my-data
//        meta.addUserMetadata("name", "my-data");

        // 上传文件
        PutObjectResponse putObjectFromFileResponse = client.putObject(bucketName, fileName, file, meta);

        // 打印ETag
//        System.out.println(putObjectFromFileResponse.getETag());
        return status;
    }

    @Override
    public boolean simpleUpload_v(InputStream inputStream, String fileName, String bucketName) {
        boolean status = true;

        // 初始化上传输入流
        ObjectMetadata meta = new ObjectMetadata();
        // 设置ContentLength大小
        meta.setContentLength(1000);
        // 设置ContentType
        meta.setContentType("application/json");
        // 设置cache-control
        meta.setCacheControl("no-cache");
        // 设置x-bce-content-crc32
        meta.setxBceCrc("crc");
        // 设置自定义元数据name的值为my-data
//        meta.addUserMetadata("name", "my-data");

        // 上传输入流
        PutObjectResponse putObjectResponseFromInputStream = client.putObject(bucketName, fileName, inputStream);

        // 打印ETag
//        System.out.println(putObjectFromFileResponse.getETag());
        return status;
    }

    /**
     * 断点续传，此处用的是分块上传和追加上传。在网络比较差的环境中，该方法会更好。
     *
     * @return
     */
    @Override
    public boolean resumeBrokenUpload_r(File file, String objectKey, String bucketName) {
        boolean status = true;

        // 追加上传，以文件形式上传Object
//        AppendObjectResponse appendObjectFromFileResponse = client.appendObject(bucketName, fileName, file);
        System.out.println("bucketName, fileName" + bucketName + " " + objectKey);
        // 开始Multipart Upload
        InitiateMultipartUploadRequest initiateMultipartUploadRequest =
                new InitiateMultipartUploadRequest(bucketName, objectKey);
        // 存储级别
//        initiateMultipartUploadRequest.withStorageClass(BosClient.STORAGE_CLASS_STANDARD_IA);
//        client.initiateMultipartUpload(initiateMultipartUploadRequest);

        InitiateMultipartUploadResponse initiateMultipartUploadResponse =
                client.initiateMultipartUpload(initiateMultipartUploadRequest);

        // 打印UploadId
        System.out.println("UploadId: " + initiateMultipartUploadResponse.getUploadId());
        System.out.println("file.length(): " + file.length());

        // 计算分块数目
        int partCount = (int) (file.length() / partSize);
        if (file.length() % partSize != 0) {
            partCount++;
        }
        // 新建一个List保存每个分块上传后的ETag和PartNumber
        List<PartETag> partETags = new ArrayList<PartETag>();

        for (int i = 0; i < partCount; i++) {
            // 获取文件流
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                // 跳到每个分块的开头
                long skipBytes = partSize * i;
                fis.skip(skipBytes);

                // 计算每个分块的大小
                long size = partSize < file.length() - skipBytes ?
                        partSize : file.length() - skipBytes;

                // 创建UploadPartRequest，上传分块
                UploadPartRequest uploadPartRequest = new UploadPartRequest();
                uploadPartRequest.setBucketName(bucketName);
                uploadPartRequest.setKey(objectKey);
                uploadPartRequest.setUploadId(initiateMultipartUploadResponse.getUploadId());
                uploadPartRequest.setInputStream(fis);
                uploadPartRequest.setPartSize(size);
                uploadPartRequest.setPartNumber(i + 1);
                UploadPartResponse uploadPartResponse = client.uploadPart(uploadPartRequest);

                // 将返回的PartETag保存到List中。
                partETags.add(uploadPartResponse.getPartETag());

                // 关闭文件
                fis.close();
                // 已上传的分块数量加1
                partUploadCount++;
                System.out.println("*&^&^$%$%$%");
            } catch (FileNotFoundException e) {
                status = false;
                partUploadCount = 0;
                e.printStackTrace();
            } catch (IOException e) {
                status = false;
                partUploadCount = 0;
                e.printStackTrace();
            }

        }
        partUploadCount = 0;
        CompleteMultipartUploadRequest completeMultipartUploadRequest =
                new CompleteMultipartUploadRequest(bucketName, objectKey, initiateMultipartUploadResponse.getUploadId(), partETags);

        // 完成分块上传
        CompleteMultipartUploadResponse completeMultipartUploadResponse =
                client.completeMultipartUpload(completeMultipartUploadRequest);

        // 打印Object的ETag
        System.out.println(completeMultipartUploadResponse.getETag());


        return status;
    }

    @Override
    public boolean resumeBrokenUpload_v(InputStream inputStream, String fileName, String bucketName) {
        return false;
    }

    @Override
    public String getFileUrl(String objectKey, String domainOfBucket) throws UnsupportedEncodingException {
        return null;
    }

    @Override
    public InputStream simpleDownloadToInputStream(String objectKey, String bucketName, String outputFileAbsolutePath) {
        // 获取Object，返回结果为BosObject对象
        BosObject object = client.getObject(bucketName, objectKey);

        // 获取ObjectMeta
        ObjectMetadata meta = object.getObjectMetadata();

        // 获取Object的输入流
        InputStream objectContent = object.getObjectContent();

        return objectContent;
    }

    @Override
    public File simpleDownloadToFile(String objectKey, String bucketName, String outputFileAbsolutePath) {
        // 新建GetObjectRequest
        System.out.println("开始下载："+bucketName+" "+objectKey);
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, objectKey);

        // 指定的输出文件
        File file = new File(outputFileAbsolutePath);

        // 下载Object到文件
        ObjectMetadata objectMetadata = client.getObject(getObjectRequest, file);

        return file;
    }

    @Override
    public boolean deleteOne(String objectKey, String bucketName) {
        //指定要删除的Object所在Bucket名称和该Object名称
        client.deleteObject(bucketName, objectKey);
        return true;
    }

    @Override
    public boolean deleteMany(List<String> objectKeys, String bucketName) {
        final int maxRequest = 1000;
        // 计算objectKeys的分批次数，每次最多为1000个删除请求
        int count = (int) (objectKeys.size() / maxRequest);
        if (objectKeys.size() % partSize != 0) {
            count++;
        }

        int start;
        int end;
        for (int i = 0; i < count; count++) {
            start = i * maxRequest;
            end = start + maxRequest;
            if (i == count - 1) {
                end = objectKeys.size();
            }

            DeleteMultipleObjectsRequest request = new DeleteMultipleObjectsRequest();
            request.setBucketName(bucketName);
            request.setObjectKeys(objectKeys.subList(start, end));
            DeleteMultipleObjectsResponse response = client.deleteMultipleObjects(request);
        }

        return false;
    }
}
