package com.uestc.piecescloud.service.cloud.qiniu;

import com.baidubce.services.bos.BosClient;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.BatchStatus;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.persistent.FileRecorder;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.service.cloud.ICloudUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.List;

/**
 * @Description: 实现部分所需的且七牛云提供商所能提供的对外接口
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 21:24
 */
public class QiniuUtil implements ICloudUtil {

    public final static String CLOUD_NAME = Constant.QINIU;

    /*
     * 对象属性
     */
    private String ACCESS_KEY_ID = "";
    private String SECRET_ACCESS_KEY = "";
    private String bucketName = "";
    private BosClient client = null;

    private Auth auth;
    private Zone zone = Zone.zone0(); //默认是z0
    private Configuration cfg;

    public QiniuUtil(String ACCESS_KEY_ID, String SECRET_ACCESS_KEY, String ENDPOINT) {
        this.ACCESS_KEY_ID = ACCESS_KEY_ID;
        this.SECRET_ACCESS_KEY = SECRET_ACCESS_KEY;
        setConfiguration();
        this.auth = qosClientInit();
    }

    /**
     * 根据ACCESS_KEY,SECRET_KEY来进行生成认证
     *
     * @return Auth
     */
    private Auth qosClientInit() {
        return Auth.create(ACCESS_KEY_ID, SECRET_ACCESS_KEY);
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    /**
     * 根据所需（bucket）设置zone Zone.zone0() 华东 Zone.zone1() 华北 Zone.zone2() 华南
     * Zone.zoneNa0() 北美
     */
    public void setConfiguration() {
        this.cfg = new Configuration(this.zone);
    }

    /**
     * 上传回复的凭证
     *
     * @param bucketName
     * @param objectKey  表示在云上的文件名
     * @return
     */
    private String getUpToken(String bucketName, String objectKey) {
        return auth.uploadToken(bucketName, objectKey, 3600, new StringMap().put("insertOnly", 0));
    }

    @Override
    public boolean simpleUpload_r(File file, String fileName, String bucketName) {
        UploadManager uploadManager = new UploadManager(this.cfg);
        ;
        try {
            Response response = uploadManager.put(file, fileName, getUpToken(bucketName, fileName));
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
//            System.out.println(putRet.hash);
//            System.out.println(putRet.key);
        } catch (QiniuException ex) {
            Response r = ex.response;
//            System.err.println("七牛云:" + r.toString());
//            System.err.println("七牛云:" + r.bodyString());
            return false;
        }
        return true;
    }

    @Override
    public boolean simpleUpload_v(InputStream inputStream, String objectKey, String bucketName) {
        UploadManager uploadManager = new UploadManager(this.cfg);
        try {
            Response response = uploadManager.put(inputStream, objectKey,
                    getUpToken(bucketName, objectKey), null, null);
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);

            System.out.println(putRet.key);
            System.out.println(putRet.hash);
        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println("七牛云:" + r.toString());
            try {
                System.err.println("七牛云:" + r.bodyString());
            } catch (QiniuException ex2) {
                // ignore
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean resumeBrokenUpload_r(File file, String fileName, String bucketName) {
        String localTempDir = Paths.get(System.getenv("java.io.tmpdir"), bucketName).toString();
        try {
            //设置断点续传文件进度保存目录
            FileRecorder fileRecorder = new FileRecorder(localTempDir);
            UploadManager uploadManager = new UploadManager(cfg, fileRecorder);
            try {
                Response response = uploadManager.put(file, fileName, auth.uploadToken(bucketName));
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                System.out.println(putRet.key);
                System.out.println(putRet.hash);
            } catch (QiniuException ex) {
                Response r = ex.response;
                System.err.println(r.toString());
                try {
                    System.err.println(r.bodyString());
                } catch (QiniuException ex2) {
                    //ignore
                }
                return false;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public boolean resumeBrokenUpload_v(InputStream inputStream, String fileName, String bucketName) {
        String localTempDir = Paths.get(System.getenv("java.io.tmpdir"), bucketName).toString();
        try {
            //设置断点续传文件进度保存目录
            FileRecorder fileRecorder = new FileRecorder(localTempDir);
            UploadManager uploadManager = new UploadManager(cfg, fileRecorder);
            try {
                Response response = uploadManager.put(inputStream, fileName, getUpToken(bucketName, fileName), null, null);
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                System.out.println(putRet.key);
                System.out.println(putRet.hash);
            } catch (QiniuException ex) {
                Response r = ex.response;
                System.err.println(r.toString());
                try {
                    System.err.println(r.bodyString());
                } catch (QiniuException ex2) {
                    //ignore
                }
                return false;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 七牛的下载是会先得到文件的URL，然后执行URL下载文件
     *
     * 私有云与公有云下载方式不一样
     *
     * @param objectKey
     *            云上文件名
     * @param domainOfBucket
     *            bucket的域名，不是bucket
     * @return
     * @throws UnsupportedEncodingException
     */
    @Override
    public String getFileUrl(String objectKey, String domainOfBucket) throws UnsupportedEncodingException {
        String encodedFileName = URLEncoder.encode(objectKey, "utf-8");
        String publicUrl = String.format("%s/%s", domainOfBucket, encodedFileName);
        long expireInSeconds = 3600;// 1小时，可以自定义链接过期时间
        String finalUrl = auth.privateDownloadUrl(publicUrl, expireInSeconds);
        System.out.println(finalUrl);
        return finalUrl;
    }

    @Override
    public InputStream simpleDownloadToInputStream(String fileName, String domainOfBucket, String outputFileAbsolutePath) {

        if (domainOfBucket == "" || domainOfBucket == null) {
            System.err.println("七牛云：domainOfBucket不能为空。");
            return null;
        }
        URL url = null;
        try {
            // 生成七牛云上链接的url类
            url = new URL(getFileUrl(fileName, domainOfBucket));
            // 将url链接下的文件以字节流的形式存储到 DataInputStream类中
            DataInputStream dataInputStream = new DataInputStream(url.openStream());

            // 处理data流
            // TODO 虚切的时候来写

            // 从所包含的输入流中读取[buffer.length()]的字节，并将它们存储到缓冲区数组buffer 中。
            // dataInputStream.read()会返回写入到buffer的实际长度,若已经读完 则返回-1

//            dataInputStream.close();// 关闭输入流
            return dataInputStream;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public File simpleDownloadToFile(String objectKey, String domainOfBucket, String outputFileAbsolutePath) {

        if (domainOfBucket == "" || domainOfBucket == null) {
            System.err.println("七牛云：domainOfBucket不能为空。");
            return null;
        }
        URL url = null;
        try {
            // 生成七牛云上链接的url类
            url = new URL(getFileUrl(objectKey, domainOfBucket));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置超时间为3秒
            conn.setConnectTimeout(3 * 1000);
            // 防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

            // 得到输入流
            InputStream inputStream = conn.getInputStream();
            // 获取自己数组
            byte[] getData = readInputStream(inputStream);

            File file = new File(outputFileAbsolutePath);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(getData);
            if (fos != null) {
                fos.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
//            System.out.println("info:" + url + " download success");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 从输入流中获取字节数组
     * @param inputStream
     * @return
     * @throws IOException
     */
    private static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

    @Override
    public boolean deleteOne(String objectKey, String bucketName) {
        /*
         * 这里有个认证，莫非指的是私有云才有？
         */
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucketName, objectKey);
        } catch (QiniuException ex) {
            // 如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteMany(List<String> files, String bucketName) {
        // setConfiguration(Zone.zone0());
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            // 单次批量请求的文件数量不得超过1000
            int limitMax = 1000;
            String[] objectKeyList = new String[files.size()];
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            if (files.size() < limitMax) {
                batchOperations.addDeleteOp(bucketName, files.toArray(objectKeyList));
            } else {
                // TODO 没测试过
                // 单次超过1000个，就会进行分段处理
                System.out.println("七牛云：待删除文件列表大于1000个，正在执行分段删除操作...");
                int count = (int) Math.ceil(objectKeyList.length / (double) limitMax);
                int fromIndex = 0;
                int toIndex = 0;
                String[] objectKeyList_2 = new String[limitMax];
                for (int i = 0; i < count; i++) {
                    fromIndex = i * limitMax + 0;
                    if (i == count - 1) {// 即为最后一段时，list中最后一段不是1000个
                        toIndex = files.size() - 1;
                    } else {
                        toIndex = i * limitMax + limitMax - 1;
                    }
                    batchOperations.addDeleteOp(bucketName, files.subList(fromIndex, toIndex).toArray(objectKeyList_2));// 第一次是删除List中的0到999
                }
            }

            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < objectKeyList.length; i++) { // 打印回调操作，不需要分段
                BatchStatus status = batchStatusList[i];
                String objectKey = objectKeyList[i];
                if (status.code == 200) {
                    System.out.println("七牛云:  objectKey:" + objectKey + " delete success");
                } else {
                    System.out.println("七牛云:  objectKey:" + objectKey + " delete fail");
                }
            }
        } catch (QiniuException ex) {
            System.err.println(ex.response.toString());
            return false;
        }
        return true;
    }
}

