package com.uestc.piecescloud.service.cloud;

import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.service.cloud.baidu.BaiduReadAdapter;
import com.uestc.piecescloud.service.cloud.baidu.BaiduWriteAdapter;
import com.uestc.piecescloud.service.cloud.baidu.BaiduUtil;
import com.uestc.piecescloud.service.cloud.jinshan.JinshanReadAdapter;
import com.uestc.piecescloud.service.cloud.jinshan.JinshanWriteAdapter;
import com.uestc.piecescloud.service.cloud.qiniu.QiniuReadAdapter;
import com.uestc.piecescloud.service.cloud.qiniu.QiniuWriteAdapter;
import com.uestc.piecescloud.service.cloud.qiniu.QiniuUtil;
import com.uestc.piecescloud.service.cloud.tengxun.TengxunReadAdapter;
import com.uestc.piecescloud.service.cloud.tengxun.TengxunWriteAdapter;
import org.springframework.stereotype.Component;

/**
 * @Description: 利用工厂模式进行实例化，仅提供给Cloud包内的类使用
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 21:38
 */
class CloudFactory {

    /**
     * 实例化一个指定云名字的云工具类
     * @param cloud 云枚举
     * @return 指定的云的写操作/上传适配器
     */
    public final static IWrite newWriteInstance(CLOUD cloud) {
        // 枚举类型，可以直接通过switch语句
        switch (cloud) {
            case BAIDU:
                return new BaiduWriteAdapter();
            case QINIU:
                return new QiniuWriteAdapter();
            case JINSHAN:
                return new JinshanWriteAdapter();
            case TENGXUN:
                return new TengxunWriteAdapter();
            default:
                return null;
        }
    }

    /**
     * 实例化一个指定云名字的云工具类
     *
     * @param cloud 云枚举
     * @return 指定的云的读操作/下载适配器
     */
    public final static IRead newReadInstance(CLOUD cloud) {
        // 枚举类型，可以直接通过switch语句
        switch (cloud) {
            case BAIDU:
                return new BaiduReadAdapter();
            case QINIU:
                return new QiniuReadAdapter();
            case JINSHAN:
                return new JinshanReadAdapter();
            case TENGXUN:
                return new TengxunReadAdapter();
            default:
                return null;
        }
    }

}
