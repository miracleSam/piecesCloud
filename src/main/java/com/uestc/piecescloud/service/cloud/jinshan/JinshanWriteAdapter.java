package com.uestc.piecescloud.service.cloud.jinshan;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.service.cloud.CloudUserConfiguration;
import com.uestc.piecescloud.service.cloud.IWrite;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.utils.Calculate;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @Description: 利用适配器模式，使云提供商所提供的方法，更好的契合项目$
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 23:21
 */
public class JinshanWriteAdapter implements IWrite {

    private JinshanUtil jinshanUtil;
    private String bucketName;
    private String domainBucket;
    private static final String cloudName = JinshanUtil.CLOUD_NAME;
    private List<FilePart> tasks;
    private CountDownLatch countDownLatch;

    @Override
    public void init() {
        if (jinshanUtil != null)
            return;
        String AKI = CloudUserConfiguration.getJinshanAKI();
        String SAK = CloudUserConfiguration.getJinshanSAK();
        this.domainBucket = CloudUserConfiguration.getJinshanDomain();
        this.bucketName = CloudUserConfiguration.getJinshanBucketName();
        jinshanUtil = new JinshanUtil(AKI, SAK, bucketName);
    }

    @Override
    public boolean simpleUpload_r(FilePart file) {
        return false;
    }

    @Override
    public boolean resumeBrokenUpload_r(FilePart filePart) {
        init();
        if (filePart != null) {
            System.out.println(JSON.toJSONString(new File(filePart.getAbsolutePath())));
            System.out.println(filePart.getName());
        }
        jinshanUtil.resumeBrokenUpload_r(new File(filePart.getAbsolutePath()), filePart.getName(), bucketName);
        return false;
    }

    @Override
    public boolean simpleUpload_v(FilePart filePart) {
        return false;
    }

    @Override
    public boolean resumeBrokenUpload_v(FilePart filePart) {
//        jinshanUtil.resumeBrokenUpload_v();
        return false;
    }

    @Override
    public boolean delete(List<FilePart> files) {
        return false;
    }

    @Override
    public void startUp(List<FilePart> tasks, CountDownLatch countDownLatch) {
        Thread thread = new Thread(this);
        this.tasks = tasks;
        this.countDownLatch = countDownLatch;
        thread.start();
    }

    @Override
    public void run() {
        // 取源文件名
        String FMID = tasks.get(0).getOriginFile().getFMID();
        // 根据源文件名和云名字初始化云任务列表,有则直接取索引,无则初始化新的地址空间
        Map<String, Object> cloudTask = FileManager.initCloudTask(FMID, cloudName);
//        "cloudName"： String
//        "transedNumber" ： int
//        "totalNumber" ： int
//        "speed" ： double (默认是KB/S)
//        "speedUnit" ： String ("MB/s KB/s B/s")
//        "maxSpeed" : double
        // 存入总的任务量
        cloudTask.put(Constant.FILE_MANAGER_TOTALNUMBER, tasks.size());
        // 记录最高速度,默认是KB/s
        double maxSpeed = 0.0;
        // 记录单个文件平均速度
        double speed = 0.0;
        // 已上传的数量
        int upedNumber = 0;
        // 计算平均速度
        long start = System.currentTimeMillis();
        for (FilePart filePart : tasks) {
            // 计算单个文件上传速度,主要用于记录最高速度
            long s = System.currentTimeMillis();
            // 启动上传方法,实切虚切只需替换这个方法即可
            resumeBrokenUpload_r(filePart);
            // 计算单个文件碎片的结束时间,如果是最后一个文件,也记录整个文件的结束时间
            long end = System.currentTimeMillis();

            // 平均速度默认以KB/s为单位
            speed = Calculate.getSpeed(start, end, filePart.getLength(),
                    cloudTask.get(Constant.FILE_MANAGER_SPEEDUNIT).toString());
            cloudTask.put(Constant.FILE_MANAGER_SPEED, speed);
            cloudTask.put(Constant.FILE_MANAGER_TRANSEDNUMBER, ++upedNumber);
            cloudTask.put(Constant.FILE_MANAGER_PROGRESS,
                    Calculate.doubleToPercent(
                            new BigDecimal((float) upedNumber / ((float) tasks.size())).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
            if (speed > maxSpeed) {
                maxSpeed = speed;
                cloudTask.put(Constant.FILE_MANAGER_MAX_SPEED, maxSpeed);
            }

        }
        // 线程同步 -1
        countDownLatch.countDown();
    }
}

