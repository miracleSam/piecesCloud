package com.uestc.piecescloud.service.cloud;

import com.uestc.piecescloud.bean.FilePart;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Description: 调用云提供商所提供的方法（写操作），通过适配器的模式进行调用，此接口主要为所需要的方法的集合$
 * @Author: 红雨澜山梦
 * @create: 2019-02-11 23:11
 */
public interface IWrite extends Runnable {

    /**
     * 初始化，实际上就是实例化一个client
     */
    void init();

    /**
     * 小文件（<5M）通过该方法更好
     *
     * @param file
     * @return
     */
    boolean simpleUpload_r(FilePart file);

    /**
     * 建议大文件通过断点续传进行传送
     *
     * @param filePart
     * @return
     */
    boolean resumeBrokenUpload_r(FilePart filePart);

    boolean simpleUpload_v(FilePart filePart);

    boolean resumeBrokenUpload_v(FilePart filePart);

    boolean delete(List<FilePart> files);

    void startUp(List<FilePart> files, CountDownLatch countDownLatch);

}
