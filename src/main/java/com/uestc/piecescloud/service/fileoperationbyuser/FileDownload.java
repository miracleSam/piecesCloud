package com.uestc.piecescloud.service.fileoperationbyuser;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.Log;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.service.cloud.CloudProviderService;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import com.uestc.piecescloud.service.user.HistoryLogService;
import com.uestc.piecescloud.utils.ApplicationContextUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @Description:
 * @Author: 红雨澜山梦
 * @Date: 2019-07-05 15:43
 */
public class FileDownload implements Runnable{

    private OriginFile originFile;

    private CloudProviderService cloudProviderService = ApplicationContextUtil.getBean(
            CloudProviderService.class);

    public FileDownload(OriginFile originFile) {
        this.originFile = originFile;
    }

    @Override
    public void run() {
        FileManager.initDownloadCloudTasks(originFile.getFMID());
        // 1.将文件状态更改为正在下载
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.DWING);
        // 2.根据源文件Id，获取下载的云任务列表
        OriginFileService originFileService = ApplicationContextUtil.getBean(OriginFileService.class);
        Map<CLOUD, List<FilePart>> cloudTasks = originFileService.queryFileParts(originFile.getOriginFileId());
        System.out.println("FileDownload start cloudTasks " + JSON.toJSONString(cloudTasks));
        // 用于存放所有的碎片文件，方便合并
        List<FilePart> mergeFileParts = new ArrayList<>();
        // 4.下载云中的碎片文件（3.根据云任务下载列表，初始化云下载任务列表）
        // 线程同步
        CountDownLatch countDownLatch = new CountDownLatch(cloudTasks.size());
        for (Map.Entry<CLOUD, List<FilePart>> entry : cloudTasks.entrySet()) {
            // 存放合并所需要的文件碎片
            mergeFileParts.addAll(entry.getValue());
            // 轮询启动下载线程
            cloudProviderService.newReadInstance(entry.getKey()).startDown(originFile, entry.getValue(), countDownLatch);
        }
        try {
            // 等待下载线程的结束
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.DW_MERGING);
        // 5.调用文件拼装机，将碎片文件拼装成源文件，这里默认源文件下载到HSB_FILE_OPERATRION_PATH路径下
        // 也改变了源文件的状态DW_MERGING->DW_MERGED
        new FileMerge(originFile, mergeFileParts).run();
        // 注意：此处文件合并机如果是开启线程，则收尾工作会提前进行

        // 6.进入文件下载机的收尾工作
        after(mergeFileParts);
        // 7.写入日志，执行下载的时候 若不是缩略图时 则写入history.log文件
        if (!originFile.isThumbnail()) {
            HistoryLogService historyLogService = new HistoryLogService(new User(originFile.getOpenid()));
            Log log = new Log();
            log.setOriginFileId(originFile.getOriginFileId());
            log.setFilename(originFile.getOriginFileName());
            log.setStatus_code(FILE_STATUS.DWED.getCode());
            log.setStatus(FILE_STATUS.DWED.getStatusName());
            log.setTimestamp(originFile.getModifyTime());
//            log.setDownload(true);
            System.out.println(JSON.toJSONString(originFile.getOpenid()));
            historyLogService.insertOneLog(log);
        }
    }
    /**
     * 下载结束后的收尾工作，主要是执行文件碎片列表的删除
     *
     * @param mergeFileParts
     */
    private void after(List<FilePart> mergeFileParts) {
//        System.out.println("mergeFileParts.get(0).getAbsolutePath():"+mergeFileParts.get(0).getAbsolutePath());
        // 删除碎片文件列表
        FileDestroy.destroyFileParts(mergeFileParts);
    }


}
