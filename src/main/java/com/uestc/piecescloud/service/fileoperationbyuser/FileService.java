package com.uestc.piecescloud.service.fileoperationbyuser;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import com.uestc.piecescloud.service.user.UserConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: 红雨澜山梦
 * @Date: 2019-07-03 19:08
 */
@Service
public class FileService {

    @Autowired
    private OriginFileService originFileService;

    public void upload(User user, File file) {
        new Thread(new UploadRunnable(user, file)).start();
    }

    public void download(User user, OriginFile originFile) {
        // 这里应该等待下载线程结束，才能返回。因为还有下载完成后，才可以传输文件到wechat
        new Thread(new DownloadRunnable(user, originFile)).run();
    }

    /**
     * 文件上传线程
     */
    class UploadRunnable implements Runnable {

        private User user;
        private File file;

        public UploadRunnable(User user, File file) {
            this.user = user;
            this.file = file;
        }

        @Override
        public void run(){
            OriginFile originFile = new OriginFile(file);
            originFile.setOpenid(user.getOpenid());
            FileManager.putOriginFileStatus(originFile, FILE_STATUS.UP_CHANGE_FINISHED);
            FileManager.putFMIDtoUserUploadMap(user, originFile);
            Map<OriginFile, List<FilePart>> tasks = new FileSplit(user, originFile).split();
            new FileUpload(tasks).run();
        }
    }

    /**
     * 文件下载线程
     */
    class DownloadRunnable implements Runnable {

        private User user ;
        private OriginFile originFile ;

        public DownloadRunnable(User user, OriginFile originFile) {
            this.user = user;
            this.originFile = originFile;
        }

        @Override
        public void run() {
            // 若下载的文件目的为了缩略图，则不进行if里面的操作
            if (!originFile.isThumbnail()) {
                originFile.setOpenid(user.getOpenid()); // 用于check是否有权限下载文件，目前没有使用check，如果是缩略图，此值为null
                originFile.setRealMerge(UserConfiguration.getIsRealMerge(user));
                FileManager.putOriginFileStatus(originFile, FILE_STATUS.DW_WAIT);
                FileManager.putFMIDtoUserDownloadMap(user, originFile);
            }
            // 下载文件缩略图 就不用放入FMID中
            else {
                originFile.setRealMerge(true); //默认是实际的合并
            }
            new FileDownload(originFile).run();
        }

    }


}




