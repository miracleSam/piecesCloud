package com.uestc.piecescloud.service.fileoperationbyuser;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;

import java.io.File;
import java.util.List;

/**
 * @Description:
 * @Author: 红雨澜山梦
 * @Date: 2019-07-09 20:05
 */
public class FileDestroy {

    /**
     * 批量删除，实切模式中，上传和下载操作所遗留的文件碎片列表。默认路径是Constant.HSB_FILE_OPERATRION_PATH
     *
     * @param fileParts
     */
    public static void destroyFileParts(List<FilePart> fileParts) {
        for (FilePart filePart : fileParts) {
            // 下载文件的时候，文件碎片并没有绝对路径的属性
            destroy(new File(filePart.getAbsolutePath()));
        }
    }


    /**
     * 删除源文件
     *
     * @param originFile
     */
    public static void destroyOriginFile(OriginFile originFile) {
        destroy(new File(originFile.getAbsolutePath()));
    }

    /**
     * 删除普通文件
     *
     * @param file
     */
    public static void destroy(File file) {
        file.delete();
    }


}
