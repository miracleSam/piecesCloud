package com.uestc.piecescloud.service.fileoperationbyuser;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.conventions.strategy.AutoChooseSuitStrategy;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.user.UserConfiguration;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 文件切割类
 * @Author: 红雨澜山梦
 * @Date: 2019-07-03 19:06
 */
class FileSplit {

    private User user;
    private OriginFile originFile;

    public FileSplit(User user, OriginFile originFile) {
        this.user = user;
        this.originFile = originFile;
    }

    public Map<OriginFile, List<FilePart>> split() {
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UP_SPLIT_WAIT);
        Map<OriginFile, List<FilePart>> tasks = null;
        // 获得User的openid
        String openid = user.getOpenid();
        // 根据openid获取配置文件的信息，得到整理成JSON数据格式的信息
        User user = new User(openid);
        boolean isRealSplit = UserConfiguration.getIsRealSplit(user);
        boolean isRealMerge = UserConfiguration.getIsRealMerge(user);
        originFile.setRealSplit(isRealSplit);
        if (originFile.isRealSplit()) {
            tasks =  realSplit();
        }
        else {
            tasks =  virtualSplit();
        }
        if (tasks == null) {
            FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UPEX);
            return tasks;
        }
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UP_WAIT);
        return tasks;
    }

    private Map<OriginFile, List<FilePart>> realSplit() {
        String partName; // 碎片文件的文件名
        long startPos; // 源文件被切分的开始的字节位置
        int count = 1; // 碎片文件的数量
        int countLen = 1; // count的位数，主要用于生成碎片文件名，如：“partName.txt.001.part”中的“001”
        int byteSize = 1; // 碎片文件的大小  最多2047M
        List<FilePart> fileParts = new ArrayList<>();

        RandomAccessFile rFile;
        OutputStream os;
        // 判断切分大小是否合法，不合法则调用默认byteSize生成方法
        byteSize = AutoChooseSuitStrategy.getFileSplitStrategy(originFile.getOriginFileByteSize());
        byte[] b = new byte[byteSize];

        count = (int) Math.ceil(originFile.getOriginFileByteSize() / (double) byteSize);
        countLen = String.valueOf(count).length();

        for (int id = 1; id <= count; id++) {
            // partName的值的例子：partName.txt.${timestamp}.001.part
            partName = originFile.getOriginFileName() + "." + System.currentTimeMillis() + "."
                    + String.format("%0" + countLen + "d", id) + "." + Constant.FILEPART_SUFFIX;
            startPos = (id - 1) * byteSize;
            try {
                rFile = new RandomAccessFile(new File(originFile.getAbsolutePath()), "r");
                rFile.seek(startPos);// 移动指针到每“段”开头
                //这里可能需要优化下
                int s = rFile.read(b);
                // 默认输出到源文件的同级目录下文件夹下
                os = new FileOutputStream(originFile.getParent() + File.separator + partName);
                os.write(b, 0, s);
                os.flush();
                os.close();

                // 构造碎片文件对象
                FilePart filePart = new FilePart();
                filePart.setSequence(id);
                filePart.setOriginFile(originFile);
                filePart.setTotalSize(count);
                filePart.setName(partName);
                filePart.setLength(new File(originFile.getParent() + File.separator + partName).length());
                filePart.setParent(originFile.getParent());
                filePart.setAbsolutePath(originFile.getParent() + File.separator + partName);
                // 加入列表
                fileParts.add(filePart);

            } catch (IOException e) {
                e.printStackTrace();
//                        logger.error("写入碎片文件异常");
                return null;
            }
        }
        Map<OriginFile, List<FilePart>> tasks = new HashMap<>();
        tasks.put(originFile, fileParts);
        return tasks;
    }

    private Map<OriginFile, List<FilePart>> virtualSplit() {
        return realSplit();
    }

}
