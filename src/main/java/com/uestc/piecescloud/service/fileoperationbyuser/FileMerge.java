package com.uestc.piecescloud.service.fileoperationbyuser;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.service.database.memory.FileManager;

import java.io.*;
import java.util.Comparator;
import java.util.List;

/**
 * @Description:
 * @Author: 红雨澜山梦
 * @Date: 2019-07-09 20:04
 */
public class FileMerge implements Runnable{

    // 源文件名
    private OriginFile originFile;
    // 碎片文件列表
    private List<FilePart> fileParts;

    public FileMerge(OriginFile originFile, List<FilePart> fileParts) {
        this.originFile = originFile;
        this.fileParts = fileParts;
    }

    @Override
    public void run() {
        if (fileParts == null || fileParts.size() == 0) {
            return;
        }
        // 通过成员变量来进行合并的，所以此处不需要传参
        startMerge();
        // 文件合并机的收尾工作
        after();
    }

    /**
     * 开始文件合并
     */
    private void startMerge() {
        // 合并后的文件，主要为了方便下载文件状态迁移
        File mergedFile = new File(originFile.getAbsolutePath());
        RandomAccessFile rFile;
        File f;
        long startPos = 0;
        long mergedFileBytesize = 0;
        for (FilePart filePart : fileParts) {
            // 因为不知道怎么用mybatis取值后，放置到FilePart的OriginFile中
            mergedFileBytesize += filePart.getLength();
        }

        try {
            rFile = new RandomAccessFile(originFile.getAbsolutePath(), "rw");
            rFile.setLength(mergedFileBytesize);

            /*
             * 此处的fileparts中的对象顺序，已经在从数据库中取出的时候，进行过排列，所以此处的顺序为正常
             */
            mergeStrategy();

            for (FilePart fp : fileParts) {
                // 通过文件碎片的信息，实例化一个File对象
                f = new File(originFile.getParent() + File.separator + fp.getName());
                // 文件输入流
                FileInputStream fs = new FileInputStream(f);
                // 随机访问文件，定位到指定字节
                rFile.seek(startPos);
                // 字节流数组b
                byte[] b = new byte[fs.available()];
                // 将字符流读入到字节流数组
                fs.read(b);
                fs.close();
                // 将字节流数组写入到随机访问文件
                rFile.write(b);
                // 开始位置的值，加上刚刚写入文件的字节大小
                startPos += f.length();
            }
            rFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }

    /**
     * 文件合并机的收尾工作
     */
    private void after() {
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.DW_MERGED);
    }

    /**
     * 正向排序
     */
    private void mergeStrategy() {
        // 根据碎片文件的序号 进行正向排序
        fileParts.sort(Comparator.naturalOrder());
    }

}
