package com.uestc.piecescloud.service.fileoperation;

/**
 * @Description: 文件操作模块的初始化以及启动类$
 * @Author: 红雨澜山梦
 * @create: 2019-03-25 16:18
 */
public class FileBase {

    public static void start() {
        FileScanner fileScanner = new FileScanner();
        fileScanner.start();
        FileSplit fileSplit_real = new FileSplit();
        fileSplit_real.start();
    }

}
