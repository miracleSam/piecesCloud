package com.uestc.piecescloud.service.fileoperation;

import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.fileoperation.queue.FileQueue;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Description: 文件扫描器。主要对/mnt文件夹下的文件进行一个扫描，以便于文件状态和文件操作的管理。$
 * @Author: 红雨澜山梦
 * @create: 2019-02-24 11:07
 */
public class FileScanner implements Runnable {

    // 文件队列
    private FileQueue fileQueue;
    // 目标文件夹
    private File targetDir;
    // 扫描线程
    private Thread thread = null;
    // 文件扫描时，过滤的后缀名
    private Set<String> excepts = new HashSet<>();
    // 已扫描监听到的文件并且等待验证完整性，File--timestamp，即文件监听器最后一次到改变的监听到的时间戳
    private Map<OriginFile, Long> changingMap = new HashMap<>();


    public FileScanner() {
        // 待扫描的文件夹
        this.targetDir = new File(Constant.HSB_FILE_OPERATRION_PATH);
        // 判断待扫描的文件夹是否存在
        if (!this.targetDir.exists()) {
            boolean s = this.targetDir.mkdirs();
            if (!s) {
                throw new RuntimeException("Failed create targetDir!");
            }
        }
        // 添加不扫描进队列的文件类型
        for (String type : Constant.SCANNER_EXCEPT_TYPES) {
            excepts.add(type);
        }
        // 初始化文件队列
        fileQueue = FileQueue.getInstance();
    }

    /**
     * 添加一个“例外”后缀，在文件扫描的时候过滤之
     *
     * @param suffix
     */
    public FileScanner addExceptSuffix(String suffix) {
        if (suffix != null) {
            if (suffix.startsWith(".")) {
                suffix = suffix.substring(1, suffix.length());
            }
            this.excepts.add(suffix);
        }
        return this;
    }

    /**
     * 移除“例外”后缀
     *
     * @param suffix
     */
    public void removeExceptSuffix(String suffix) {
        this.excepts.remove(suffix);
    }

    /**
     * 将符合条件的文件添加进队列中
     *
     * @param f
     */
    private void addFile2Queue(File f) {
        if (fileQueue != null) {
            // 转换成OriginFile对象
            OriginFile originFile = new OriginFile(f);
            // 文件是否符合进入队列的条件
            if (!havaStatus(f)) {
                if (!isExcept(f)) {
                    // 此处获取并设置该文件的上传方式
                    originFile.setRealSplit(new FileOpeartionUserConfiguration().isRealSplit());
                    // 不直接添加到文件队列，而是添加到一个待定的map中，该map主要是为了保证文件的完整性
                    testifyFileIntegrityMap(originFile);
                } else {
                    // 若不可上传的后缀匹配成功，赋予999状态
                    FileManager.putOriginFileStatus(originFile, FILE_STATUS.EXCEPT);
                }
            }
        }
    }

    /**
     * 将文件监听器和scanFiles()方法所监听和扫描到的文件，添加到changinglist中，进行一个暂存，
     * 待文件已经确保完整的传输到树莓派后，再自动的传入Queue中
     */
    private void testifyFileIntegrityMap(OriginFile originFile) {
        // 在正在改变的map中，增加源文件的最后一次更新时间（create or change）
        // change 也会覆盖源文件列表中的信息
        changingMap.put(originFile, System.currentTimeMillis());
        // 若成功添加到完整性验证队列，赋予000状态
        // 由于putOriginFileStatus内部已经判别是否存在该originFile的状态，所以此处就算put已有的源文件，也不会有影响
        FileManager.putOriginFileStatus(originFile, FILE_STATUS.UP_CHANGEING);
    }

    /**
     * 检查该文件是否应该被添加到文件队列中
     *
     * @param f
     * @return true--进入 false--不可进入
     */
    private boolean isExcept(File f) {
        boolean isExcept = false;
        for (String e : excepts) {
            // 如果后缀名是被“过滤”的后缀名
            if (f.getName().endsWith("." + e)) {
                isExcept = true;
                break;
            }
        }
        // TODO 检查该文件是否处于下载状态s
        return isExcept;
    }

    /**
     * 检查文件是否在文件生命周期中已经拥有状态
     *
     * @param f
     * @return
     */
    private boolean havaStatus(File f) {
        return FileManager.isExist(f.getName());
    }

    /**
     * 开始扫描
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
//			logger.info("开启文件切片线程");
        }
    }

    @Override
    public void run() {
//        logger.info("···开始扫描文件夹：" + targetDir.getName());
        scanFiles(targetDir);
//        logger.info("···结束扫描文件夹：" + targetDir.getName());
        listenDir();
        // 每隔1秒检测一次changingMap中的时间戳，10秒内若无改变，则将其加入filequeue中
        // 此处没有对文件的大小进行一个时间划分的判断，比如1MB以下，2秒内时间没改变，则认为文件已经可以加入切片队列，1GB以上，需要3秒内没变，则认为文件已经通过验证
        Thread testify = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    // 取map中的元素，这里我们使用迭代器
                    Iterator<OriginFile> iterator = changingMap.keySet().iterator();
                    while (iterator.hasNext()) {
                        // 取带检查的key文件
                        OriginFile originFile = iterator.next();
                        // 进行时间的校验，比10秒大，则认为传输完成且可靠
                        if (System.currentTimeMillis() - changingMap.get(originFile) >= 3000) {
                            // push进入文件队列
                            fileQueue.push(originFile);
                            // 更新源文件状态
                            FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UP_CHANGE_FINISHED);
                            System.out.println("FileScanner:添加了文件" + originFile.getOriginFileName());
                            iterator.remove(); // 移除当前指针所指的元素
                        }
                    }
                    // 执行完for后，休眠1秒
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        // 启动验证线程
        testify.start();
    }

    /**
     * 递归扫描指定的文件夹（只有文件扫描机在初始化的时候才会执行）
     *
     * @param file
     */
    private void scanFiles(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                // 如果是文件夹，递归扫描
                if (f.isDirectory()) {
                    scanFiles(f);
                } else {
                    // 如果是文件，添加进文件队列
                    addFile2Queue(f);
                }
            }
        } else {
            addFile2Queue(file);
        }
    }

    /**
     * 监听文件夹
     */
    private void listenDir() {
        // 轮询间隔 1 秒
        long interval = TimeUnit.SECONDS.toMillis(1);
        // 创建一个文件观察器用于处理文件的格式
        // FileAlterationObserver observer = new
        // FileAlterationObserver(directory, FileFilterUtils.and(
        // FileFilterUtils.fileFileFilter(),FileFilterUtils.suffixFileFilter(".txt")));
        FileAlterationObserver observer = new FileAlterationObserver(targetDir);
        // 设置文件变化监听器
        observer.addListener(new MyFileAlterationListener());
        FileAlterationMonitor monitor = new FileAlterationMonitor(interval, observer);
        try {
            monitor.start();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("文件夹变化监听失败");
        }
    }


    /**
     * 文件变化监听器
     *
     * @author 蓝亭书序
     */
    class MyFileAlterationListener implements FileAlterationListener {

        @Override
        public void onStart(FileAlterationObserver fileAlterationObserver) {
            // System.out.println("monitor start scan files..");
        }

        @Override
        public void onDirectoryCreate(File file) {
//            logger.info(file.getAbsolutePath() + " director created.");
        }

        @Override
        public void onDirectoryChange(File file) {
//            logger.info(file.getAbsolutePath() + " director changed.");
        }

        @Override
        public void onDirectoryDelete(File file) {
//            logger.info(file.getAbsolutePath() + " director deleted.");
        }

        @Override
        public void onFileCreate(File file) {
//            logger.info(file.getAbsolutePath() + " created.");
            addFile2Queue(file);
            System.out.println("add file : " + file.getAbsolutePath());
        }

        @Override
        public void onFileChange(File file) {
//            logger.info(file.getAbsolutePath() + " changed.");
            addFile2Queue(file);
            System.out.println("create file : " + file.getAbsolutePath());
        }

        @Override
        public void onFileDelete(File file) {
//            logger.info(file.getAbsolutePath() + " deleted.");
            //TODO 删除文件暂时不移除源文件内存状态表，文件删除与移除应该分开控制
//            FileManager.remove(file.getName());
            System.out.println("delete file : " + file.getAbsolutePath());
        }

        @Override
        public void onStop(FileAlterationObserver fileAlterationObserver) {
            // System.out.println("monitor stop scanning..");
        }
    }

}
