package com.uestc.piecescloud.service.fileoperation;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.service.database.memory.FileManager;

import java.io.File;
import java.util.List;

/**
 * @Description: 本地文件删除机$
 * @Author: 红雨澜山梦
 * @create: 2019-03-25 20:32
 */
public class FileDestroy {

    /**
     * 批量删除，实切模式中，上传和下载操作所遗留的文件碎片列表。默认路径是Constant.HSB_FILE_OPERATRION_PATH
     *
     * @param fileParts
     */
    public static void destroyFileParts(List<FilePart> fileParts) {
        for (FilePart filePart : fileParts) {
            // 下载文件的时候，文件碎片并没有绝对路径的属性
//            destroy(new File(filePart.getAbsolutePath()));
//            destroy(new File(Constant.HSB_FILE_OPERATRION_PATH + File.separator + filePart.getName()));
            destroy(new File(filePart.getAbsolutePath()));
        }
    }

    /**
     * 批量删除，删除指定文件夹下的碎片文件列表
     *
     * @param fileParts 待删除的文件碎片列表
     * @param parent    其父目录的路径
     */
    public static void destroyFileParts(List<FilePart> fileParts, String parent) {
        for (FilePart filePart : fileParts) {
            // 下载文件的时候，文件碎片并没有绝对路径的属性
            destroy(new File(parent + File.separator + filePart.getName()));
        }
    }

    /**
     * 删除源文件
     *
     * @param originFile
     */
    public static void destroyOriginFile(OriginFile originFile) {
        destroy(new File(originFile.getAbsolutePath()));
    }

    /**
     * 删除普通文件
     *
     * @param file
     */
    public static void destroy(File file) {
        file.delete();
    }
}
