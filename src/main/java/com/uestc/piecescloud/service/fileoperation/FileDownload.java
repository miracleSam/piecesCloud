package com.uestc.piecescloud.service.fileoperation;

import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.service.cloud.CloudProviderService;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import com.uestc.piecescloud.utils.ApplicationContextUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @Description: 文件下载机（云 -> HSB ？->Front End），进行文件的下载功能，目前以碎片化的模式进行下载和拼装，一次下载一个文件
 * @Author: 红雨澜山梦
 * @create: 2019-03-28 19:42
 */
public class FileDownload implements Runnable {

    // 源文件Id（必须在数据库能找到该id）
    private int originFileId;
    // 源文件名字（可以与数据库信息不一样）
    private String originFileName;
    // 是否传输到前端
    private boolean toFrontEnd = false;

    private CloudProviderService cloudProviderService = ApplicationContextUtil.getBean(
            CloudProviderService.class);

    public FileDownload(int originFileId, String originFileName) {
        this.originFileId = originFileId;
        this.originFileName = originFileName;
    }

    public FileDownload(int originFileId, String originFileName, boolean toFrontEnd) {
        this(originFileId, originFileName);
        this.toFrontEnd = true;
    }

    @Override
    public void run() {
        OriginFile originFile = new OriginFile(originFileName);
        // 设置源文件的合并方式， 目前恒为false
        originFile.setAppendMerge(new FileOpeartionUserConfiguration().isAppendMerge());
        // 0.初始化，同时得到FMID存放于originFile中
        FileManager.putOriginFileStatus(originFile, FILE_STATUS.DW_WAIT);
        FileManager.initDownloadCloudTasks(originFile.getFMID());
        // 1.将文件状态更改为正在下载
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.DWING);
        // 2.根据源文件Id，获取下载的云任务列表
        OriginFileService originFileService = ApplicationContextUtil.getBean(OriginFileService.class);
        Map<CLOUD, List<FilePart>> cloudTasks = originFileService.queryFileParts(originFileId);
        // 用于存放所有的碎片文件，方便合并
        List<FilePart> mergeFileParts = new ArrayList<>();
        // 4.下载云中的碎片文件（3.根据云任务下载列表，初始化云下载任务列表）
        // 线程同步
        CountDownLatch countDownLatch = new CountDownLatch(cloudTasks.size());
        for (Map.Entry<CLOUD, List<FilePart>> entry : cloudTasks.entrySet()) {
            // 存放合并所需要的文件碎片
            mergeFileParts.addAll(entry.getValue());
            // 轮询启动下载线程
            cloudProviderService.newReadInstance(entry.getKey()).startDown(originFile, entry.getValue(), countDownLatch);
        }
        try {
            // 等待下载线程的结束
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.DW_MERGING);
        // 5.调用文件拼装机，将碎片文件拼装成源文件，这里默认源文件下载到HSB_FILE_OPERATRION_PATH路径下
        // 也改变了源文件的状态DW_MERGING->DW_MERGED
        new FileMerge(originFile, mergeFileParts).run();
        // 注意：此处文件合并机如果是开启线程，则收尾工作会提前进行

        // 6.进入文件下载机的收尾工作
        after(mergeFileParts);
        // 7.将文件状态置为”已经下载到HSB，并准备传输到本机“（本机指前端）
        // 文件下载机下载到HSB，无第⑦步
//        if(toFrontEnd) {
//            FileManager.updateOriginFileStatus(originFile, FILE_STATUS.DW_TO_WECHAT); //目前只有微信
//        }
    }

    /**
     * 下载结束后的收尾工作，主要是执行文件碎片列表的删除
     *
     * @param mergeFileParts
     */
    private void after(List<FilePart> mergeFileParts) {
        // 删除碎片文件列表
        FileDestroy.destroyFileParts(mergeFileParts, Constant.HSB_BUFFER_PATH);
    }
}
