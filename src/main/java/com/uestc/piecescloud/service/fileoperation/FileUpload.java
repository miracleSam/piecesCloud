package com.uestc.piecescloud.service.fileoperation;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.conventions.strategy.AutoChooseSuitStrategy;
import com.uestc.piecescloud.service.cloud.CloudProviderService;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.database.storage.OriginFileService;
import com.uestc.piecescloud.service.fileoperation.queue.FilePartsQueue;
import com.uestc.piecescloud.utils.ApplicationContextUtil;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @Description: 文件上传机，本类无模式区分，上传模式区分在云上传线程中。$
 * @Author: 红雨澜山梦
 * @create: 2019-02-24 17:18
 */
class FileUpload implements Runnable {
    private FilePartsQueue filePartsQueue = FilePartsQueue.getInstance();
    // 待上传任务列表，只有一个键值对
    private Map<OriginFile, List<FilePart>> tasks;

    private CloudProviderService cloudProviderService = ApplicationContextUtil.getBean(CloudProviderService.class);

    private OriginFileService originFileService = ApplicationContextUtil.getBean(OriginFileService.class);

    @Override
    public void run() {
        this.tasks = getTasks();
        if (this.tasks == null) {
            return;
        }
        // 上传任务
        Map<CLOUD, List<FilePart>> uploads = autoChooseSuitStrategy(this.tasks);
        OriginFile originFile = null;
        // 初始化云任务列表,并且将文件状态置为正在上传
        // 由于只有一个键值对,故只执行一次
        for (OriginFile of : tasks.keySet()) {
            originFile = of;
            FileManager.initUploadCloudTasks(of.getFMID());
            // 文件正在上传，更改文件状态 赋予102code
            FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UPING);
        }
        // 执行上传操作,此处不进行模式传递的原因是：模式已经在OriginFile中设定,上传线程取出碎片文件列表,可从碎片文件对象访问源文件对象
        execute(uploads);
        // 执行上传操作，启动的是线程，此处若线程执行没有结束，而删除了文件，就会出现问题
        //TODO 默认循环执行成功,执行数据库的插入操作
        originFileService.insertOriginFile(originFile, uploads);
        // 文件上传完成，更改文件状态 赋予103code
        FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UPED);
        // 上传结束后的收尾工作
        after(originFile, uploads);
    }

    /**
     * 上传结束后的收尾工作，主要是执行文件的删除和源文件的状态
     *
     * @param originFile
     * @param uploads
     */
    private void after(OriginFile originFile, Map<CLOUD, List<FilePart>> uploads) {
        // 删除源文件
//        FileDestroy.destroyOriginFile(originFile);
        // 删除碎片文件列表
        for (Map.Entry<CLOUD, List<FilePart>> entry : uploads.entrySet()) {
            FileDestroy.destroyFileParts(entry.getValue());
        }
    }

    /**
     * 执行上传操作
     *
     * @param uploads
     */
    private void execute(Map<CLOUD, List<FilePart>> uploads) {
        // 线程同步
        CountDownLatch countDownLatch = new CountDownLatch(uploads.size());
        // 循环调度云
        for (Map.Entry<CLOUD, List<FilePart>> entry : uploads.entrySet()) {
            System.out.println(entry.getKey() + JSON.toJSONString(entry.getValue()));
            // 注意：当碎片数量小于云的数量时，部分云下的任务就会为null，即无任务，此时continue即可
            if (entry.getValue() == null || entry.getValue().size() == 0) {
                // 此处一定要countDown，因为在实例化的时候，定义了包括空任务列表的云
                countDownLatch.countDown();
                continue;
            }
            // 通过多态和线程,调用各个云平台上传端口
            cloudProviderService.newWriteInstance(entry.getKey()).startUp(entry.getValue(), countDownLatch);
        }
        try {
            // 等待上传线程的结束
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 自动选择上传策略
     *
     * @param tasks
     * @return
     */
    private Map<CLOUD, List<FilePart>> autoChooseSuitStrategy(Map<OriginFile, List<FilePart>> tasks) {

//        OriginFile originFile = null;
        List<FilePart> fileParts = null;
        // 只取第一个，因为只有一个
        for (OriginFile f : tasks.keySet()) {
//            originFile = f;
            fileParts = tasks.get(f);
            break;
        }
        return AutoChooseSuitStrategy.getFileUploadStrategy(fileParts);
    }

    /**
     * 获取任务
     *
     * @return
     */
    private Map<OriginFile, List<FilePart>> getTasks() {
        return filePartsQueue.pop();
    }
}
