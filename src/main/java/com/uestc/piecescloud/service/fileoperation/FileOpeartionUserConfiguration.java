package com.uestc.piecescloud.service.fileoperation;

/**
 * @Description: 配置文件操作模块的文件切分，合并的模式$
 * @Author: 红雨澜山梦
 * @create: 2019-03-26 14:51
 */
public class FileOpeartionUserConfiguration {

    private boolean isRealSplit = true;  // false 虚切  true 实切
    private boolean isAppendMerge = false;  // false 普通拼接(下载成一个个实际的碎片文件)  true 追加拼接(文件流直接拼接)

    public void setRealSplit(boolean isRealSplit) {
        this.isRealSplit = isRealSplit;
    }

    public void setAppendMerge(boolean isAppendMerge) {
        this.isAppendMerge = isAppendMerge;
    }

    /**
     * 根据用户的等级获取切分的模式
     *
     * @return 是否为实切
     */
    public boolean isRealSplit() {

        // TODO 根据用户的账号等级或者其余配置设置是否为实切。此处暂时随机分配。
        // ...
        setRealSplit(queryConfiguration());
        return isRealSplit;
    }

    public boolean isAppendMerge() {

        // TODO 根据用户的账号等级或者其余配置设置是否为虚拼。
        // ...
        setAppendMerge(false);
        return isAppendMerge;
    }

    /**
     * 查找配置，此处直接随机选取
     */
    private boolean queryConfiguration() {
        int result = ((int) (10 * Math.random())) % 2;
        if (result == 0) {
            return false;
        } else {
            return true;
        }
    }




}
