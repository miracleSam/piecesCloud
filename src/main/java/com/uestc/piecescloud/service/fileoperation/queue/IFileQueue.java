package com.uestc.piecescloud.service.fileoperation.queue;

/**
 * 文件队列接口。
 *
 * @author 蓝亭书序
 */
public interface IFileQueue<T> {

    /**
     * 从头部取出一个元素出来，如果为空，则等待
     *
     * @return
     */
    T pop();

    /**
     * 从尾部添加一个元素进去，如果队列满，则等待
     *
     * @param f
     */
    void push(T f);

    /**
     * 从队列中移除指定的文件
     *
     * @param f
     * @return
     */
    T remove(T f);

    /**
     * 从队列中移除指定索引的文件
     *
     * @param index
     * @return
     */
    T remove(int index);

    boolean isEmpty();

}
