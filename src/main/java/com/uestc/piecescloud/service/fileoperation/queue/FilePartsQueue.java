package com.uestc.piecescloud.service.fileoperation.queue;


import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author 红雨澜山梦
 * @date 2018-04-10 下午4:12:51
 */
public class FilePartsQueue {

    private static FilePartsQueue filePartsQueue = new FilePartsQueue();
    // 线程安全的队列对象
    private static LinkedBlockingQueue<Map<OriginFile, List<FilePart>>> queue = new LinkedBlockingQueue<>();

    private FilePartsQueue() {
    }

    /**
     * 获取单例对象
     *
     * @return
     */
    public static FilePartsQueue getInstance() {
        return filePartsQueue;
    }

    public Map<OriginFile, List<FilePart>> pop() {
        Map<OriginFile, List<FilePart>> fileListMap = null;
        try {
            fileListMap = queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return fileListMap;
    }

    public void push(Map<OriginFile, List<FilePart>> fileListMap) {
        if (fileListMap != null) {
            try {
                queue.put(fileListMap);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public Map<File, List<FilePart>> remove(Map<File, List<FilePart>> fileListMap) {
        boolean s = queue.remove(fileListMap);
        if (s) {
            return fileListMap;
        }
        return null;
    }

    public Map<File, List<FilePart>> remove(int index) {
        if (index < queue.size() && index >= 0) {
            int i = 0;
            Object obj = null;
            for (Object o : queue) {
                if (i == index) {
                    obj = o;
                    break;
                }
                i++;
            }
            return remove((Map<File, List<FilePart>>) obj);
        }
        return null;
    }

    public boolean isEmpty() {
        if (queue.size() != 0)
            return false;
        return true;
    }

}
