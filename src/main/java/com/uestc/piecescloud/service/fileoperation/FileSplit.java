package com.uestc.piecescloud.service.fileoperation;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.conventions.strategy.AutoChooseSuitStrategy;
import com.uestc.piecescloud.conventions.strategy.filesplit.FilePartSize;
import com.uestc.piecescloud.service.database.memory.FileManager;
import com.uestc.piecescloud.service.fileoperation.queue.FilePartsQueue;
import com.uestc.piecescloud.service.fileoperation.queue.FileQueue;
import com.uestc.piecescloud.utils.Time;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description: 文件切片机，含有 文件实切类 和 文件虚切类$
 * @Author: 红雨澜山梦
 * @create: 2019-02-24 16:22
 */
public class FileSplit implements Runnable {

    private Thread thread = null;
    private FileQueue fileQueue = FileQueue.getInstance(); //文件队列
    private FilePartsQueue filePartsQueue = FilePartsQueue.getInstance(); // 碎片文件队列

    public FileSplit() {
    }

    /**
     * 开始切片
     */
    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
//			logger.info("开启文件切片线程");
        }
    }

    @Override
    public void run() {
        // 利用线程池启动上传线程
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
        while (true) {
            // 源文件，这里是从文件队列fileQueue中pop一个文件下来
            // 取文件，若队列为空，一秒后再取
            OriginFile originFile = fileQueue.pop();
            // 准备等待切片
            FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UP_SPLIT_WAIT);
            if (originFile != null) {
                if (originFile.isRealSplit())
                    // 实切
                    fileSplitRealAndAddToQueue(originFile);
                else
                    // 虚切
                    fileSplitVirtualAndAddToQueue(originFile);
                // 切片完成，准备上传
                FileManager.updateOriginFileStatus(originFile, FILE_STATUS.UP_WAIT);
                // 利用线程池启动线程
                fixedThreadPool.execute(new FileUpload());

            } else {
                Time.threadSleepInTimeMillis(1000);
            }
        }
    }

    private void fileSplitRealAndAddToQueue(OriginFile originFile) {

        String partName; // 碎片文件的文件名
        long startPos; // 源文件被切分的开始的字节位置
        int count = 1; // 碎片文件的数量
        int countLen = 1; // count的位数，主要用于生成碎片文件名，如：“partName.txt.001.part”中的“001”
        int byteSize = 1; // 碎片文件的大小  最多2047M
        List<FilePart> fileParts = new ArrayList<>();

        RandomAccessFile rFile;
        OutputStream os;
        // 判断切分大小是否合法，不合法则调用默认byteSize生成方法
        byteSize = AutoChooseSuitStrategy.getFileSplitStrategy(originFile.getOriginFileByteSize());
        byte[] b = new byte[byteSize];

        count = (int) Math.ceil(originFile.getOriginFileByteSize() / (double) byteSize);
        countLen = String.valueOf(count).length();

        for (int id = 1; id <= count; id++) {
            // partName的值的例子：partName.txt.${timestamp}.001.part
            partName = originFile.getOriginFileName() + "." + System.currentTimeMillis() + "."
                    + String.format("%0" + countLen + "d", id) + "." + Constant.FILEPART_SUFFIX;
            startPos = (id - 1) * byteSize;
            try {
                rFile = new RandomAccessFile(new File(originFile.getAbsolutePath()), "r");
                rFile.seek(startPos);// 移动指针到每“段”开头
                //这里可能需要优化下
                int s = rFile.read(b);
                // 默认输出到源文件的同级目录下文件夹下
                os = new FileOutputStream(originFile.getParent() + File.separator + partName);
                os.write(b, 0, s);
                os.flush();
                os.close();

                // 构造碎片文件对象
                FilePart filePart = new FilePart();
                filePart.setSequence(id);
                filePart.setOriginFile(originFile);
                filePart.setTotalSize(count);
                filePart.setName(partName);
                filePart.setLength(new File(originFile.getParent() + File.separator + partName).length());
                filePart.setAbsolutePath(originFile.getParent() + File.separator + partName);
                // 加入列表
                fileParts.add(filePart);

            } catch (IOException e) {
                e.printStackTrace();
//                        logger.error("写入碎片文件异常");
                return;
            }
        }
        Map<OriginFile, List<FilePart>> fileListMap = new HashMap<>();
        fileListMap.put(originFile, fileParts);
        addFilePartsMap2Queue(fileListMap);
    }

    private void fileSplitVirtualAndAddToQueue(OriginFile originFile) {
        //TODO 为了可扩展性，暂时使用实切
        fileSplitRealAndAddToQueue(originFile);
    }

    /**
     * 添加到文件到碎片文件的映射
     *
     * @param fileListMap 映射的引用
     */
    private void addFilePartsMap2Queue(Map<OriginFile, List<FilePart>> fileListMap) {
        System.out.println("文件切片机：" + JSON.toJSONString(fileListMap));
        filePartsQueue.push(fileListMap);
    }


}
