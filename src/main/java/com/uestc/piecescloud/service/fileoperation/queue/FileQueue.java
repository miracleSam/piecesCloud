package com.uestc.piecescloud.service.fileoperation.queue;

import com.uestc.piecescloud.bean.OriginFile;

import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 文件队列的实现类。<br/>
 * 当文件扫描机扫描出文件后，将文件推送进文件队列.为了防止出现push和pop操作在同一个对象上，本类使用单例模式
 *
 * @author 蓝亭书序
 */
public class FileQueue {

    private static FileQueue fileQueue = new FileQueue();
    // 线程安全的队列对象
    private static LinkedBlockingQueue<OriginFile> queue = new LinkedBlockingQueue<>();

    private FileQueue() {
    }

    /**
     * 获取单例对象
     *
     * @return
     */
    public static FileQueue getInstance() {
        return fileQueue;
    }

    public OriginFile pop() {
        OriginFile originFile = null;
        try {
            originFile = queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return originFile;
    }

    public void push(OriginFile originFile) {
        if (originFile != null) {
            try {
                queue.put(originFile);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public OriginFile remove(OriginFile originFile) {
        boolean s = queue.remove(originFile);
        if (s) {
            return originFile;
        }
        return null;
    }

    public OriginFile remove(int index) {
        if (index < queue.size() && index >= 0) {
            int i = 0;
            Object obj = null;
            for (Object o : queue) {
                if (i == index) {
                    obj = o;
                    break;
                }
                i++;
            }
            return remove((OriginFile) obj);
        }
        return null;
    }

    public boolean isEmpty() {
        if (queue.size() != 0)
            return false;
        return true;
    }

}
