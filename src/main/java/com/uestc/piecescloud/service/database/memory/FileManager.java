package com.uestc.piecescloud.service.database.memory;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
//import com.uestc.piecescloud.service.database.storage.LogService;
import com.uestc.piecescloud.utils.ApplicationContextUtil;
import com.uestc.piecescloud.utils.MD5Utils;
import com.uestc.piecescloud.utils.UnitConvert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: 红雨澜山梦
 * @Date: 2019/2/25 22:33
 * @Version: 0.0.1
 */
public class FileManager {

    /*
     * ${FMID}-- originFileInfoInMemory
     * -----------------------------------------
     * ---------originFileInfoInMemory:
     * ------------"status" : FILE_STATUS
     * ------------"originFileName" : String
     * ------------"code" : FILE_STATUS.code
     * ------------"cloudTasks" : List<Map>
     * ------------------List<Map>中的map:
     * ------------------------"cloudName"： String
     * ------------------------"transedNumber" ： int
     * ------------------------"totalNumber" ： int
     * ------------------------"speed" ： float (默认是KB/S)
     * ------------------------"speedUnit" ： String ("MB/s KB/s B/s")
     * -----------------------------------------------------------------
     * ------------transedNumber -- ${transedNumber} (int)
     * ------------totalNumber -- ${totalNumber} (int)
     * ------------speed -- ${speed} (double)((默认是KB/S))
     * ------------speedUnit -- ${speedUnit} (String ("MB/s KB/s B/s"))
     * ------------progress -- ${progress} (double)
     * ------------isExist -- ${progress} (boolean)
     */
    private static Map<String, Map<String, Object>> originFileInfoMap = new HashMap<>();


    /*
     * ${openid} -- "upload" : FMIDs
     *              "download" : FMIDs
     */
    private static Map<String, Map<String, List<String>>> userOriginFileMemoryIdMap = new HashMap<>();




    private FileManager(){}

    /**
     * 查询所有文件状态管理机中的所有上传操作的文件状态信息状态
     *
     * @return 所有上传操作的文件状态信息状态映射
     */
    private static void generateFMID(OriginFile originFile) {
        if (originFile.getFMID() == null || originFile.getFMID() == "") {
            originFile.setFMID(
                    MD5Utils.MD5StringHashCode32(originFile.getOriginFileName() + System.currentTimeMillis()));
        }

    }

    public static Map<String, Map<String, Object>> getUploadMap() {
        Map<String, Map<String, Object>> uploadMap = new HashMap<>();
        for (Map.Entry<String, Map<String, Object>> entry : originFileInfoMap.entrySet()) {
            // 获取状态码
            String code = entry.getValue().get(Constant.FILE_MANAGER_CODE).toString();
            if (FILE_STATUS.isUP(code)) {
                uploadMap.put(entry.getKey(), entry.getValue());
            }
        }
        /*
         * 添加所有的总计的信息
         */
        addTotalSpeedAndNumber(uploadMap);
        return uploadMap;
    }

    /**
     * 浅克隆,查询所有文件状态管理机中的所有下载操作的文件状态信息状态
     *
     * @return 所有下载操作的文件状态信息状态映射
     */
    public static Map<String, Map<String, Object>> getDownloadMap() {
        Map<String, Map<String, Object>> downloadMap = new HashMap<>();
        for (Map.Entry<String, Map<String, Object>> entry : originFileInfoMap.entrySet()) {
            // 获取状态码
            String code = entry.getValue().get(Constant.FILE_MANAGER_CODE).toString();
            if (FILE_STATUS.isDW(code)) {
                downloadMap.put(entry.getKey(), entry.getValue());
            }
        }
        /*
         * 添加所有的总计的信息
         */
        addTotalSpeedAndNumber(downloadMap);
        return downloadMap;
    }

    /**
     * 浅克隆,查询所有文件状态管理机中的所有正在进行操作的文件状态信息状态
     * @return 所有正在进行操作的文件状态信息状态映射
     */
    public static Map<String, Map<String, Object>> getAllMap() {
        Map<String, Map<String, Object>> allMap = new HashMap<>();
        for (Map.Entry<String, Map<String, Object>> entry : originFileInfoMap.entrySet()) {
            allMap.put(entry.getKey(), entry.getValue());
        }
        /*
         * 添加所有的总计的信息
         */
        addTotalSpeedAndNumber(allMap);
        return allMap;
    }

    /**
     * 根据输入的info Map表中的每个cloud的传输信息,计算出总计的速度值,已经传输的总个数和所有需要传输的个数
     *
     * @param transMap
     */
    private static void addTotalSpeedAndNumber(Map<String, Map<String, Object>> transMap) {
        /*
         *transedNumber ： int
         * totalNumber  ： int
         * totalSize    ： long (默认是B)
         * transedSize  ： long (默认是B)
         *       speed  ： double (默认是KB/S)
         *   speedUnit  ： String ("MB/s KB/s B/s")
         *    progress  ： double
         */
        Map<String, Object> totalMap = new HashMap<>();

        for (Map.Entry<String, Map<String, Object>> entry : transMap.entrySet()) {
            int totalNumber = 0;
            int transedNumber = 0;
            // 单位为B
            long totalSize = 0L;
            long transedSize = 0L;
            double totalSpeed = 0.0;
            String speedUnit = "KB/s";
            Map<String, Object> originFileInfoInMemory = entry.getValue();

            List<Map<String, Object>> cloudTasks =
                    (List<Map<String, Object>>) (originFileInfoInMemory.get(Constant.FILE_MANAGER_CLOUDTASKS));
            if (cloudTasks == null || cloudTasks.isEmpty() || cloudTasks.size() == 0) {
                totalMap.put(Constant.FILE_MANAGER_TOTALNUMBER, totalNumber);
                totalMap.put(Constant.FILE_MANAGER_TRANSEDNUMBER, transedNumber);
//                totalMap.put(Constant.FILE_MANAGER_TOTALSIZE, totalSize);
//                totalMap.put(Constant.FILE_MANAGER_TRANSEDSIZE, transedSize);
                totalMap.put(Constant.FILE_MANAGER_SPEED, totalSpeed);
                totalMap.put(Constant.FILE_MANAGER_SPEEDUNIT, speedUnit);
                // 进度条
                totalMap.put(Constant.FILE_MANAGER_PROGRESS, 0.0);
                /*
                 * 置入原键值对中
                 */
                entry.getValue().putAll(totalMap);
                continue;
            }
            for (Map<String, Object> cloudTask : cloudTasks) {
                totalNumber += (int) cloudTask.get(Constant.FILE_MANAGER_TOTALNUMBER);
                transedNumber += (int) cloudTask.get(Constant.FILE_MANAGER_TRANSEDNUMBER);
//                totalSize += (long) cloudTask.get(Constant.FILE_MANAGER_TOTALSIZE);
//                transedSize += (long) cloudTask.get(Constant.FILE_MANAGER_TRANSEDSIZE);
                speedUnit = (String) cloudTask.get(Constant.FILE_MANAGER_SPEEDUNIT);
                totalSpeed += Double.parseDouble(cloudTask.get(Constant.FILE_MANAGER_SPEED).toString());
            }

            totalMap.put(Constant.FILE_MANAGER_TOTALNUMBER, totalNumber);
            totalMap.put(Constant.FILE_MANAGER_TRANSEDNUMBER, transedNumber);
//            totalMap.put(Constant.FILE_MANAGER_TOTALSIZE, totalSize);
//            totalMap.put(Constant.FILE_MANAGER_TRANSEDSIZE, transedSize);
            totalMap.put(Constant.FILE_MANAGER_SPEED, new BigDecimal(totalSpeed).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            totalMap.put(Constant.FILE_MANAGER_SPEEDUNIT, speedUnit);
            // 进度条
            totalMap.put(Constant.FILE_MANAGER_PROGRESS,
                    UnitConvert.floatToPercent(
                            new BigDecimal((float) transedNumber / (float) totalNumber).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
            /*
             * 置入原键值对中
             */
            entry.getValue().putAll(totalMap);
        }

    }

    /**
     * 为正在上传的源文件,初始化云任务列表
     *
     * @param FMID 源文件FMID
     */
    public static void initUploadCloudTasks(String FMID) {
        if (!isExist(FMID)) {
            Map<String, Object> status = new HashMap<>();
            status.put(Constant.FILE_MANAGER_STATUS, FILE_STATUS.UPING.getStatusName());
            status.put(Constant.FILE_MANAGER_CODE, FILE_STATUS.UPING.getCode());
            originFileInfoMap.put(FMID, status);
        }
        /*
         * 在originFIleName所对应的键值对中,初始化cloudTasks键值对
         */
        originFileInfoMap.get(FMID).put(Constant.FILE_MANAGER_CLOUDTASKS, new ArrayList<>());
    }

    /**
     * 在cloudTasks中初始化一个名为cloudName的指定的云任务
     *
     * @param FMID 源文件FMID
     * @param cloudName      云名字
     * @return 指定的云列表
     */
    public static Map<String, Object> initCloudTask(String FMID, String cloudName) {
        List<Map<String, Object>> cloudTasks =
                (List<Map<String, Object>>) originFileInfoMap.get(FMID).get(Constant.FILE_MANAGER_CLOUDTASKS);
        // 检查是否已经对cloudName进行过初始化了
        for (Map<String, Object> cloudTask : cloudTasks) {
            // 若含有直接return
            if (cloudTask.containsKey(cloudName)) {
                return cloudTask;
            }
        }
        // 这步肯定是不含有cloudName,则创建
        Map<String, Object> cloudTask = new HashMap<>();
        cloudTask.put(Constant.FILE_MANAGER_CLOUD_NAME, cloudName);
        cloudTask.put(Constant.FILE_MANAGER_SPEED, 0.0);
        cloudTask.put(Constant.FILE_MANAGER_TRANSEDNUMBER, 0);
        cloudTask.put(Constant.FILE_MANAGER_TOTALNUMBER, 0);
        cloudTask.put(Constant.FILE_MANAGER_SPEEDUNIT, "KB/s");
        cloudTask.put(Constant.FILE_MANAGER_MAX_SPEED, 0.0);
        // put created cloudTask to originFileInfoMap
        ((List<Map<String, Object>>) originFileInfoMap.get(FMID)
                .get(Constant.FILE_MANAGER_CLOUDTASKS)).add(cloudTask);
        return cloudTask;
    }

    /**
     * 为正在下载的源文件,初始化云任务列表
     *
     * @param FMID
     */
    public static void initDownloadCloudTasks(String FMID) {
        if (!isExist(FMID)) {
            Map<String, Object> status = new HashMap<>();
            status.put(Constant.FILE_MANAGER_STATUS, FILE_STATUS.DWING.getStatusName());
            status.put(Constant.FILE_MANAGER_CODE, FILE_STATUS.DWING.getCode());
            originFileInfoMap.put(FMID, status);
        }
        /*
         * 在originFIleName所对应的键值对中,初始化cloudTasks键值对
         */
        originFileInfoMap.get(FMID).put(Constant.FILE_MANAGER_CLOUDTASKS, new ArrayList<>());
    }

    /**
     * 名为name的源文件，是否已经拥有一个状态
     *
     * @param FMID 源文件在内存的ID
     * @return true--存在 false--不存在
     */
    public static boolean isExist(String FMID) {
        return originFileInfoMap.containsKey(FMID);
    }

    /**
     * 将源文件的信息添加到源文件信息map中。若该源文件已存在于源文件信息map中，则不插入；否则插入。
     * 注意：不会进行更新操作。
     * @param originFile 源文件
     * @param status     源文件状态
     */
    public static void putOriginFileStatus(OriginFile originFile, FILE_STATUS status) {
        generateFMID(originFile);
        // 为该originFile创建属于它自己的内存信息表
        Map<String, Object> originFileInfoInMemory = new HashMap<>();
        // 在新建的内存信息表中放入状态信息键值对
        originFileInfoInMemory.put(Constant.FILE_MANAGER_STATUS, status);
        originFileInfoInMemory.put(Constant.FILE_MANAGER_CODE, status.getCode());
        originFileInfoInMemory.put(Constant.FILE_MANAGER_ORIGINFILENAME, originFile.getOriginFileName());
        originFileInfoInMemory.put(Constant.FILE_MANAGER_ORIGINFILE_ABSOLUTE_PATH, originFile.getAbsolutePath());
        // 在源文件信息表中，存放源文件FMID与它对应的内存信息表的键值对
        originFileInfoMap.put(originFile.getFMID(), originFileInfoInMemory);
    }

    /**
     * 将源文件信息map中的源文件的信息更新。若该源文件已存在于源文件信息map中，则更新；否则插入。
     *
     * @param originFile 源文件
     * @param status     更新后的源文件状态
     */
    public static void updateOriginFileStatus(OriginFile originFile, FILE_STATUS status) {
        if (isExist(originFile.getFMID())) {
            originFileInfoMap.get(originFile.getFMID())
                    .put(Constant.FILE_MANAGER_STATUS, status);
            originFileInfoMap.get(originFile.getFMID())
                    .put(Constant.FILE_MANAGER_CODE, status.getCode());
        } else {
            // 若不存在，则put
            putOriginFileStatus(originFile, status);
        }
    }

    /**
     * 根据源文件的文件名，移除源文件信息表中的键值对
     *
     * @param FMID
     */
    public static void remove(String FMID) {
        if (isExist(FMID)) {
//            if (originFileInfoMap.get(FMID).get(Constant.FILE_MANAGER_CODE) != "999") {
//                originFileInfoMap.get(FMID).put("originFileName", FMID);
//                originFileInfoMap.get(FMID).put("modifyTime", System.currentTimeMillis());
                // 移除的状态需要存入数据库中，方便查询已完成的记录
//                LogService logService =
//                        ApplicationContextUtil.getBean(LogService.class);
                // 插入日志信息
//                logService.insertOriginFileStatusLog(originFileInfoMap.get(FMID));
//            }
            // 垃圾回收装置不知道可以不可以自动回收
//            originFileInfoMap.get(name).clear();
            // 插入完成后，删除文件状态表在内存中的信息
            originFileInfoMap.remove(FMID);
        }
    }


    private static void initUserFMIDsMap(User user) {
        if (!userOriginFileMemoryIdMap.containsKey(user.getOpenid())) {
            Map<String, List<String>> transFMIDsMap = new HashMap<>();
            transFMIDsMap.put(Constant.FILE_MANAGER_UPLOAD, new ArrayList<String>());
            transFMIDsMap.put(Constant.FILE_MANAGER_DOWNLOAD, new ArrayList<String>());
            userOriginFileMemoryIdMap.put(user.getOpenid(), transFMIDsMap);
        }
    }

    public static void putFMIDtoUserUploadMap(User user, OriginFile originFile) {
        getFMIDsUploadMapByUser(user).add(originFile.getFMID());
    }

    public static void putFMIDtoUserDownloadMap(User user, OriginFile originFile) {
        getFMIDsDownloadMapByUser(user).add(originFile.getFMID());
    }

    public static void removeFMIDfromUserUploadMap(User user, OriginFile originFile) {
        getFMIDsUploadMapByUser(user).remove(originFile.getFMID());
    }

    public static void removeFMIDfromUserDownloadMap(User user, OriginFile originFile) {
        getFMIDsDownloadMapByUser(user).remove(originFile.getFMID());
    }

    private static List<String> getFMIDsUploadMapByUser(User user) {
        initUserFMIDsMap(user);
        return userOriginFileMemoryIdMap.get(user.getOpenid()).get(Constant.FILE_MANAGER_UPLOAD);
    }

    private static List<String> getFMIDsDownloadMapByUser(User user) {
        initUserFMIDsMap(user);
        return userOriginFileMemoryIdMap.get(user.getOpenid()).get(Constant.FILE_MANAGER_DOWNLOAD);
    }

    public static Map<String, Map<String, Object>> getUploadMapByUser(User user) {
        List<String> FMIDs = getFMIDsUploadMapByUser(user);
        Map<String, Map<String, Object>> uploadMap = getMapByFMIDs(FMIDs);
        addTotalSpeedAndNumber(uploadMap);
        return uploadMap;
    }

    public static Map<String, Map<String, Object>> getDownloadMapByUser(User user) {
        List<String> FMIDs = getFMIDsDownloadMapByUser(user);
        Map<String, Map<String, Object>> downloadMap = getMapByFMIDs(FMIDs);
        addTotalSpeedAndNumber(downloadMap);
        return downloadMap;
    }

    private static Map<String, Map<String, Object>> getMapByFMIDs(List<String> FMIDs) {
        Map<String, Map<String, Object>> map = new HashMap<>();
        for (Map.Entry<String, Map<String, Object>> entry : originFileInfoMap.entrySet()) {
            // 可能在FMIDs中会有部分未匹配到的FMID，不过不影响功能性
            if (FMIDs.contains(entry.getKey())) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
        return map;
    }

}




