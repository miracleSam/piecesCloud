package com.uestc.piecescloud.service.database.storage;

import com.alibaba.fastjson.JSON;
import com.jn.sqlhelper.dialect.pagination.PagingRequest;
import com.jn.sqlhelper.dialect.pagination.SqlPaginations;
import com.uestc.piecescloud.bean.FilePart;
import com.uestc.piecescloud.bean.Log;
import com.uestc.piecescloud.bean.OriginFile;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.constant.Constant;
import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;
import com.uestc.piecescloud.dao.*;
import com.uestc.piecescloud.service.user.HistoryLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: 调用底层数据持久化层，对外提供关于源文件数据的服务$
 * @Author: 红雨澜山梦
 * @create: 2019-02-02 00:54
 */
@Service
public class OriginFileService {
    @Resource
    private OriginFileDao originFileDao;
    @Resource
    private FilePartDao filePartDao;
    @Resource
    private Lv1IndexDao lv1IndexDao;
    @Resource
    private Lv2IndexDao lv2IndexDao;
    @Resource
    private CloudDao cloudDao;
    @Resource
    private UserDao userDao;

    /**
     * 根据文件类型查询指定的源文件信息
     * @param type
     * @return
     */
    public List<Map<String, Object>> queryAllOriginFile(String type, User user,Integer pageNum) {
        // 根据前端发送时附加的文件类型，得到相应的后缀
        // 查询所有源文件信息
        PagingRequest request = SqlPaginations.preparePagination(pageNum==null? 1:pageNum,6);
        List<OriginFile> originFiles = originFileDao.queryAll(getTypes(type), user);
//        System.out.println("查询文件："+JSON.toJSONString(originFiles));
        // 需要返回的源文件信息最终表
        List<Map<String, Object>> originFileInfos = new ArrayList<>();
        // 源文件信息表的单个元素
        Map<String, Object> originFileInfo;
        for (OriginFile originFile : originFiles) {
            Date date=new Date(originFile.getModifyTime());
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String d=simpleDateFormat.format(date);
            originFileInfo = new HashMap<>();
            // 获取对应的文件的碎片信息
            originFileInfo.putAll(getFilePartsCount(originFile.getOriginFileId()));
            // 源文件信息
            originFileInfo.putAll(JSON.parseObject(JSON.toJSONString(originFile)));
            originFileInfo.put("date",d);
            originFileInfos.add(originFileInfo);
        }
        System.out.println("查询文件："+originFileInfos.toString());
        return originFileInfos;

    }

    private String[] getTypes(String type) {
        switch (type.toLowerCase()) {
            case "img":
                return Constant.IMG_TYPES;
            case "all":
                return null;
            default:
                return null;
        }
    }

    @Transactional
    public void insertOriginFile(OriginFile originFile, Map<CLOUD, List<FilePart>> tasks) {

        if (originFile == null || tasks == null || tasks.size() == 0)
            return;

        // 获取待插入的源文件id
        originFileDao.insertOne(originFile);
        int originFileId = originFile.getOriginFileId();
        // 获取待插入的userId
        User user = userDao.hadUser(new User(originFile.getOpenid()));
        userDao.insertUserOriginFile(user.getUserId(), originFileId);
//        System.out.println("user_originfile插入成功！"+user.getUserId()+" "+originFileId);
        // 获取待插入的文件碎片的id列表
        List<FilePart> fileParts = new ArrayList<>();
        for (Map.Entry<CLOUD, List<FilePart>> entry : tasks.entrySet()) {
            fileParts.addAll(entry.getValue());
        }
        // 若无数据,则返回
        if (fileParts.size() == 0) {
            throw new NullPointerException("待插入的文件碎片列表无数据!!");
        }
        // 利用事务管理进行批次插入,此处用insertMany不会返回所有主键值
        for (FilePart fp : fileParts) {
            filePartDao.insertOne(fp);
        }
        // 插入一级索引
        lv1IndexDao.insert(originFileId, fileParts);
        // 插入二级索引
        // 待插入的数据
        for (Map.Entry<CLOUD, List<FilePart>> entry : tasks.entrySet()) {
            // 若取不到值，则continue
            List<FilePart> values = entry.getValue();
            if (values == null || values.size() == 0) {
                continue;
            }
            int cid = cloudDao.queryIdByName(entry.getKey().getName());
            System.out.println(JSON.toJSONString(values));
            lv2IndexDao.insert(cid, values);
        }

        // 插入日志
        HistoryLogService historyLogService = new HistoryLogService(user);
        Log log = new Log();
        log.setOriginFileId(originFile.getOriginFileId());
        log.setFilename(originFile.getOriginFileName());
        log.setTimestamp(originFile.getModifyTime());
        log.setStatus(FILE_STATUS.UPED.getStatusName());
        log.setStatus_code(FILE_STATUS.UPED.getCode());
//        log.setUpload(true);
        historyLogService.insertOneLog(log);

//        return originFileId;
    }

    @Transactional
    public boolean deleteOriginFile(){
        boolean status = false;

        return status;
    }

    /**
     * 查询该源文件ID所对应所有碎片文件列表以及其碎片文件所各自对应的云信息（用于文件下载机的下载）
     *
     * @param originFileId 源文件id
     * @return 云名字--碎片文件列表
     */
    public Map<CLOUD, List<FilePart>> queryFileParts(int originFileId) {
        // 查询该源文件所拥有的所有碎片文件
        List<FilePart> fileParts = filePartDao.queryFilePartsByOriginFileId(originFileId);
        // 需要返回的云任务下载列表
        Map<CLOUD, List<FilePart>> cloudTasks = new HashMap<>();
        for (FilePart filePart : fileParts) {
            CLOUD cloud = CLOUD.valueOf(filePart.getCloudName().toUpperCase());
            // 若该云，是第一次添加其下的碎片文件，则实例化一个ArrayList
            if (!cloudTasks.containsKey(cloud)) {
                cloudTasks.put(cloud, new ArrayList<>());
            }
            // 添加碎片文件
            cloudTasks.get(cloud).add(filePart);
        }
        return cloudTasks;
    }

    /**
     * 查询该源文件ID所对应的所有碎片的个数以及在哪些云上的个数
     *
     * @param originFileId
     * @return map结构如下
     * key        |      value
     * total		 |		(int)
     * cloudList   |      map
     * --------------------------以下是cloud中的可选字段
     * baidu	 	 |      (int)
     * qiniu		 |		(int)
     * ...
     */
    private Map<String, Object> getFilePartsCount(int originFileId) {
        // 云名字列表
        List<String> cloudNames = cloudDao.queryAll();
        // 记录数据库中每个云所拥有该源文件的碎片数量
        Map<String, Integer> counts = new HashMap<>();
        // 最终需要return的碎片文件数量统计map
        Map<String, Object> filePartsCount = new HashMap<>();
        int total = 0;
        for (String cloudName : cloudNames) {
//            System.out.println(cloudName + " " + originFileId);
            int count = filePartDao.getFilePartsCount(cloudName, originFileId);
//            System.out.println("count = " + count);
            if (count != 0) {
                // 将查询到的碎片数量，put进入counts
                counts.put(cloudName, count);
                total += count;
            }
        }
        // 总共的碎片文件数量
        filePartsCount.put("total", total);
        filePartsCount.put("cloudList", counts);

        return filePartsCount;
    }
}
