package com.uestc.piecescloud.service.database.storage;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.dao.CloudDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description: 对数据库中云的数据的服务层的操作$
 * @Author: 红雨澜山梦
 * @create: 2019-03-04 20:14
 */
@Service
public class CloudService {

    @Resource
    private CloudDao cloudDao;

    public List<String> queryAllWorkable() {
        return cloudDao.queryAllWorkable();
    }

}
