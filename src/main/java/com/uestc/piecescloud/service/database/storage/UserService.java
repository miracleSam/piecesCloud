package com.uestc.piecescloud.service.database.storage;

import com.alibaba.fastjson.JSON;
import com.uestc.piecescloud.bean.User;
import com.uestc.piecescloud.dao.UserDao;
import com.uestc.piecescloud.service.user.UserConfiguration;
import org.apache.commons.lang.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class UserService {

    @Resource
    private UserDao userDao;

    public User login(User user) {
        System.out.println("用户请求登录："+ JSON.toJSONString(user));

        User temp =  userDao.hadUser(user);
        if(temp == null){
            register(user);
            // 注册后，继续调用hadUser，返回注册时候的全部信息
            temp =  userDao.hadUser(user);
            System.out.println("user:"+user.getOpenid()+"注册成功");
        }
        // 若刚刚注册，则创建  若以前注册过，则校验下是否存在这些文件
        UserConfiguration.initial(temp);
        System.out.println("user:"+user.getOpenid()+"登录成功");
        return temp;
    }

    public void register(User user) {
        userDao.register(user);
        System.out.println("注册新用户成功："+ JSON.toJSONString(user));
    }

    public void modify() {}

    public void addFriend(int userId1, int userId2) {
        // TODO 需要先检查该关系是否已经存在于数据库
        userDao.addFriend(userId1, userId2);
    }

    public User showGetPut(User user) {
        return userDao.hadUser(user);
    }

    public User showSpace(User user) {
        return userDao.hadUser(user);
    }

    public List<User> showFriends(User user) {

        user = userDao.hadUser(user);

        List<Integer> friendIds = new ArrayList<>();
        // 将返回的结果，整理成List<Integer>，这个表示其朋友ID的列表
        List<HashMap<String, Object>> friendships = userDao.showFriends(user);
        for (HashMap<String, Object> friendship : friendships) {
            if (Integer.valueOf(friendship.get("user_id1").toString()) == user.getUserId()) {
                friendIds.add(Integer.valueOf(friendship.get("user_id2").toString()));
            }
            else {
                friendIds.add(Integer.valueOf(friendship.get("user_id1").toString()));
            }
        }

        List<User> friends = userDao.queryUsersByIds(friendIds);
        return friends;
    }

}
