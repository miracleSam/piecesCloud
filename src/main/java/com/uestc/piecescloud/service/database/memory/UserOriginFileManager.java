package com.uestc.piecescloud.service.database.memory;

import com.uestc.piecescloud.bean.OriginFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主要用于user_originfile表的管理，创建于用户发送文件时的操作，销毁于文件被删除或者被替代
 * @Author: 红雨澜山梦
 * @Date: 2019/7/2 10:28
 * @Version: 0.0.1
 */
public class UserOriginFileManager {

    /**
     * openid -- {"upload":[FMID1,FMID2,...],"download":[FMID1,FMID2,....]}
     */
    private static Map<String, Map<String, List<String>>> userOriginFileMap = new HashMap<>();

    private UserOriginFileManager() {}


}
