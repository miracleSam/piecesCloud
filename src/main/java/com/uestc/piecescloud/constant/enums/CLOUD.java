package com.uestc.piecescloud.constant.enums;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Description: 碎片云中所需要用到的枚举类型$
 * @Author: 红雨澜山梦
 * @create: 2019-02-02 01:39
 */
public enum CLOUD{

    BAIDU("baidu", 1), QINIU("qiniu", 2),
    JINSHAN("jinshan", 3), TENGXUN("tengxun", 4), ALI("ali", 5);
    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private CLOUD(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName(){
        return name;
    }

    /**
     * 获取枚举类型的list形式
     * @return
     */
    public final static List<String> list() {
        List<String> list = new ArrayList<String>();
        for(CLOUD cloud : CLOUD.values()){
            list.add(cloud.name);
        }
        return list;
    }

    /**
     * 将传入的CLOUD的列表,Set<String>
     *
     * @param clouds
     * @return
     */
    public final static Set<String> convertToSet(Set<CLOUD> clouds) {
        Set<String> set = new HashSet<>();
        for (CLOUD cloud : clouds) {
            set.add(cloud.name);
        }
        return set;
    }

//    public static void main(String[] args) {
//        String a = "baidu";
//        System.out.println(CLOUD.valueOf(a.toUpperCase()));
//        System.out.println(a);
//    }
}


