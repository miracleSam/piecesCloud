package com.uestc.piecescloud.constant.enums;

/**
 * @Description: 关于碎片云中文件状态的枚举$
 * @Author: 红雨澜山梦
 * @create: 2019-03-08 14:33
 */
public enum FILE_STATUS {

    /*
     * 前两位字母表示了该文件,处于上传还是下载的状态
     *
     * code:第一位表示的是何种状态
     * -----上传
     * 0：上传前的准备
     * 1：正式进入上传操作
     *
     * -----下载
     * 2：正式进入下载操作
     *
     * -----异常
     * 9：文件处于异常状态
     *
     */
    UP_CHANGEING("准备资源", "000"),
    UP_CHANGE_FINISHED("准备资源完毕", "001"),
    UP_SPLIT_WAIT("准备切片", "100"),
    UP_WAIT("准备上传", "101"),
    UPING("正在上传", "102"),
    UPED("上传完成", "103"),
    UPEX("上传异常", "199"),

    DW_WAIT("等待下载", "200"),
    DWING("正在下载", "201"),
    DWED("下载完成", "202"),
    DW_MERGING("正在合并", "203"),
    DW_MERGED("合并完成", "204"),
    DW_TO_WECHAT("HSB已接收，准备传输到本地", "205"),
    DW_WECHAT_SUCCESS("下载完成", "206"),
    DWEX("下载异常", "299"),

    EXCEPT("ignore", "999");

    private String statusName;
    private String code;

    public String getStatusName() {
        return statusName;
    }

    public String getCode() {
        return code;
    }

    private FILE_STATUS(String statusName, String code) {
        this.statusName = statusName;
        this.code = code;
    }

    /**
     * 该状态码是否为上传状态
     *
     * @return true--是 false--不是
     */
    public static boolean isUP(String code) {
        return code.substring(0, 1).equals("0") || code.substring(0, 1).equals("1");
    }

    /**
     * 该状态码是否为下载状态
     *
     * @return true--是 false--不是
     */
    public static boolean isDW(String code) {
        return code.substring(0, 1).equals("2");
    }

}
