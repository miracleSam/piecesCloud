package com.uestc.piecescloud.constant;

import com.uestc.piecescloud.constant.enums.CLOUD;
import com.uestc.piecescloud.constant.enums.FILE_STATUS;

/**
 * @Description: 定义了系统中会用到的一些常量信息，且不能写在配置文件中的一些关键常量$
 * @Author: 红雨澜山梦
 * @create: 2019-01-30 16:31
 */
public final class Constant {

    // HSB文件上传路径（扫描，上传，切片，删除）
    public static final String HSB_FILE_OPERATRION_PATH = "/mnt";
    // HSB文件下载路径（下载，合并）
    public static final String HSB_BUFFER_PATH = "/HSBbuffer";
    // HSB存放图片缩略图的位置
    public static final String HSB_TMP_VIEW_PATH = HSB_BUFFER_PATH + "/tmp_view";

    // HSB中存放所有用户的文件的目录
//    public static final String HSB_USER = "/users";
    public static final String HSB_USER="C:\\Users\\67409\\Desktop\\user";
    // 以下的目录表示/users/{user}/upload的路径，为了统一所有用户的操作目录名称
    public static final String HSB_USER_UPLOAD = "/upload";
    public static final String HSB_USER_DOWNLOAD = "/download";
    public static final String HSB_USER_TMP_VIEW = "/tmp_view";
    public static final String HSB_USER_SETTING = "setting.xml";
    public static final String HSB_USER_HISTORY = "history.log";
    // HSB_TMP_VIEW_PATH中缩略图的前缀 all表示主界面的缩略图 up表示上传缩略图 down表示的是下载的缩略图
    public static final String HSB_USER_TMP_VIEW_ALL = "/all";
    public static final String HSB_USER_TMP_VIEW_UP = "/upload";
    public static final String HSB_USER_TMP_VIEW_DOWN = "/download";

    // wechat文件存放，下载的路径
    public static final String WECHAT_UPLOAD_PATH = HSB_FILE_OPERATRION_PATH;
    public static final String WECHAT_DOWNLOAD_PATH = HSB_BUFFER_PATH;


    // 云提供商的名字
    public static final String BAIDU = CLOUD.BAIDU.getName();
    public static final String QINIU = CLOUD.QINIU.getName();
    public static final String JINSHAN = CLOUD.JINSHAN.getName();
    public static final String TENGXUN = CLOUD.TENGXUN.getName();
    public static final String ALI = CLOUD.ALI.getName();

    // 文件后缀
    public static final String FILEPART_SUFFIX = "part";
    public static final String[] IMG_TYPES = {"bmp","jpg","jpeg","png","gif","emf","tga","tif","rle","pcx","cdr"};
    public static final String[] SCANNER_EXCEPT_TYPES = {FILEPART_SUFFIX, "metadata"};

    // JSON数据中的key
    public static final String FILE_MANAGER_ORIGINFILENAME = "originFileName";
    public static final String FILE_MANAGER_STATUS = "status";
    public static final String FILE_MANAGER_CODE = "code";
    public static final String FILE_MANAGER_CLOUDTASKS = "cloudTasks";
    public static final String FILE_MANAGER_TOTALNUMBER = "totalNumber";
    public static final String FILE_MANAGER_SPEED = "speed";
    public static final String FILE_MANAGER_SPEEDUNIT = "speedUnit";
    public static final String FILE_MANAGER_PROGRESS = "progress";
    public static final String FILE_MANAGER_CLOUD_NAME = "cloudName";
    public static final String FILE_MANAGER_MAX_SPEED = "maxSpeed";
    public static final String FILE_MANAGER_TRANSEDNUMBER = "transedNumber";
    public static final String FILE_MANAGER_ORIGINFILE_ABSOLUTE_PATH = "originFileAbsolutePath";
    public static final String FILE_MANAGER_TOTALSIZE = "totalSize";
    public static final String FILE_MANAGER_TRANSEDSIZE = "transedSize";
    public static final String FILE_MANAGER_UPLOAD = "upload";
    public static final String FILE_MANAGER_DOWNLOAD = "download";
}
