package com.uestc.piecescloud.bean;

import java.io.File;

/**
 * @author Redain
 * @date 2018-04-26 下午2:50:50
 * 
 */
public class OriginFile {

	private String FMID;
	private int originFileId;
	private String originFileName;
	private String originFileHash;
	private long originFileByteSize;
	private long modifyTime;
	private String fileType;
	private String absolutePath;
	private String parent;
	private boolean isRealSplit; // true--实切 false--虚切
	private boolean isRealMerge; // true--实拼 false--虚拼（文件追加合并）
    private boolean isAppendMerge; // true--文件流追加合并 false--实际的碎片文件拼接而成
	private String openid;
	private boolean isThumbnail = false;

	public OriginFile() {
	}

	public OriginFile(String originFileName) {
		this.originFileName = originFileName;
	}

	public OriginFile(File file) {
		this.convert(file);
	}

	public OriginFile convert(File file) {
		this.setOriginFileName(file.getName());
		this.setOriginFileByteSize(file.length());
		this.setParent(file.getParent());
		this.setAbsolutePath(file.getAbsolutePath());
		this.setModifyTime(file.lastModified());
		this.setFileType(file.getName().substring(file.getName().lastIndexOf(".") + 1));
//		this.setOriginFileHash();
		return this;
	}

	public String getFMID() {
		return FMID;
	}

	public void setFMID(String FMID) {
		this.FMID = FMID;
	}

	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public int getOriginFileId() {
		return originFileId;
	}
	public void setOriginFileId(int originFileId) {
		this.originFileId = originFileId;
	}
	public String getOriginFileName() {
		return originFileName;
	}
	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}
	public String getOriginFileHash() {
		return originFileHash;
	}
	public void setOriginFileHash(String originFileHash) {
		this.originFileHash = originFileHash;
	}
	public long getOriginFileByteSize() {
		return originFileByteSize;
	}
	public void setOriginFileByteSize(long originFileByteSize) {
		this.originFileByteSize = originFileByteSize;
	}
	public long getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(long modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public boolean isRealSplit() {
		return isRealSplit;
	}

	public void setRealSplit(boolean realSplit) {
		isRealSplit = realSplit;
	}

    public boolean isAppendMerge() {
        return isAppendMerge;
    }

    public void setAppendMerge(boolean appendMerge) {
        isAppendMerge = appendMerge;
    }

	public boolean isRealMerge() {
		return isRealMerge;
	}

	public void setRealMerge(boolean realMerge) {
		isRealMerge = realMerge;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public boolean isThumbnail() {
		return isThumbnail;
	}

	public void setThumbnail(boolean thumbnail) {
		isThumbnail = thumbnail;
	}
}
