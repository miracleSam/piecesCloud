package com.uestc.piecescloud.bean;

/**
 * @Description: 云实体类$
 * @Author: 红雨澜山梦
 * @create: 2019-03-04 20:24
 */
public class Cloud {
    private int id;
    private String name;
    private boolean status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
