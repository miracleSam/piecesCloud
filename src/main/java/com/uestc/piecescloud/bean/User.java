package com.uestc.piecescloud.bean;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private int userId;
    private String wxid;
    private String openid;
    private String phone;
    private String username;

    private int get;
    private int put;
    private float usedSpace;
    private float totalSpace;

    private List<User> friends;

    public User() {}

    public User(String openid) {
        this();
        this.openid = openid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getWxid() {
        return wxid;
    }

    public void setWxid(String wxid) {
        this.wxid = wxid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getGet() {
        return get;
    }

    public void setGet(int get) {
        this.get = get;
    }

    public int getPut() {
        return put;
    }

    public void setPut(int put) {
        this.put = put;
    }

    public float getUsedSpace() {
        return usedSpace;
    }

    public void setUsedSpace(float usedSpace) {
        this.usedSpace = usedSpace;
    }

    public float getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(float totalSpace) {
        this.totalSpace = totalSpace;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }
}
