package com.uestc.piecescloud.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * @Description: log实体类
 * @Author: 红雨澜山梦
 * @create: 2019-04-02 01:23
 */
public class Log {

    private int originFileId;
    private long timestamp;
    private String filename;
    private String status_code;
    private String status;
//    private boolean upload = false;
//    private boolean download = false;

    public Log() {}

//    /**
//     * logInfos的顺序有要求，id，timestamp，fileName，status_code，status，upload，download
//     * @param logInfos 读取文件一行的字符串内容
//     */
//    public Log(JSONObject logJsonObject) {
//        JSON.parseObject(logJsonObject.toJSONString(), Log.class);
//    }

    public int getOriginFileId() {
        return originFileId;
    }

    public void setOriginFileId(int originFileId) {
        this.originFileId = originFileId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public boolean isUpload() {
//        return upload;
//    }
//
//    public void setUpload(boolean upload) {
//        this.upload = upload;
//    }
//
//    public boolean isDownload() {
//        return download;
//    }
//
//    public void setDownload(boolean download) {
//        this.download = download;
//    }

    @Override
    public String toString(){
        return ""+this.originFileId+" "+this.timestamp+" "+this.filename+" "+this.status_code+" "+this.status;
    }

    public String generateLineInfo(String regex) {
        return ""+this.originFileId+regex+this.timestamp+regex+this.filename+regex+this.status_code+regex+this.status;
    }
}
