package com.uestc.piecescloud.bean;

import java.io.File;

/**
 * 线性切割得到的碎片文件的实体类
 * @author Redain
 * @date 2018-04-10 上午10:56:42
 * 
 */
public class FilePart implements Comparable<FilePart> {

    private int filePartId;
    // 碎片文件的属性(此处没有声明File对象，主要是为了虚切考虑
    private String name;
    private String absolutePath;
    private String parent;
    private long length;
    private String hash;  //碎片文件的hash值
    private long modifyTime;
    private String cloudName;
    // 源文件的属性
	private OriginFile originFile;    //源文件名 如：filename.txt
	private String originFileHash;//源文件的hash值
	// 用于记录碎片文件的序号
	private int totalSize; // 文件总共被切碎的个数
	private int sequence;       //碎片文件的序号 如：1
	// 用于虚切
	private long startPos; // 表示的是源文件的切割位置，可利用上述成员变量进行计算得到，这样算有点复杂，不如让它在切片机中自动赋值
	private long endPos;
    // 上传下载是否完成
    private boolean isTransed;

    public int getFilePartId() {
        return filePartId;
    }

    public void setFilePartId(int filePartId) {
        this.filePartId = filePartId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
	}

    public long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getCloudName() {
        return cloudName;
    }

    public void setCloudName(String cloudName) {
        this.cloudName = cloudName;
    }

	public OriginFile getOriginFile() {
		return originFile;
	}

	public void setOriginFile(OriginFile originFile) {
		this.originFile = originFile;
	}

	public String getOriginFileHash() {
		return originFileHash;
	}

	public void setOriginFileHash(String originFileHash) {
		this.originFileHash = originFileHash;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public long getStartPos() {
		return startPos;
	}

	public void setStartPos(long startPos) {
		this.startPos = startPos;
	}

	public long getEndPos() {
		return endPos;
	}

	public void setEndPos(long endPos) {
		this.endPos = endPos;
	}

	public boolean isFinal() {
		return this.sequence == this.totalSize;
	}

    public boolean isTransed() {
        return isTransed;
    }

    public void setTransed(boolean transed) {
        isTransed = transed;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
        this.absolutePath = parent + File.separator + this.name;
    }

    @Override
    public int compareTo(FilePart filePart) {
        if (this.sequence >= filePart.getSequence()) {
            return 1;
        }
        // 否则 小于
        return -1;
    }
}
